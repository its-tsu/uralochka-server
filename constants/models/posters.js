module.exports = {
    types: [
        {
            fieldName: "hallOfFame",
            ruName: "Зал славы"
        }, {
            fieldName: "supportGroup",
            ruName: "Группа поддержки"
        },  {
            fieldName: "school",
            ruName: "Школа"
        }, {
            fieldName: "shop",
            ruName: "Магазин"
        }, {
            fieldName: "hallOfHistory",
            ruName: "Зал истории"
        }, {
            fieldName: "hallOfModernity",
            ruName: "Зал современности"
        }, {
            fieldName: "less13",
            ruName: "Девушки до 13 лет",
        }, {
            fieldName: "main",
            ruName: "Главная страница",
        }, {
            fieldName: "hi",
            ruName: "Приветственное слово",
        }
    ]
};
