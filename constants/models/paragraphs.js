module.exports.types = [ {
    fieldName: "less13",
    ruName: "Девушки до 13 лет",
}];

module.exports.positions = [ {
    fieldName: "top",
    ruName: "Текст сверху от фото.",
}, {
    fieldName: "bottom",
    ruName: "Текст снизу от фото.",
}];