module.exports = {
    types: [
        {
            fieldName: "hallOfFame",
            ruName: "Зал славы"
        }, {
            fieldName: "president",
            ruName: "Президент"
        }, {
            fieldName: "boardOfDirectors",
            ruName: "Совет директоров"
        }, {
            fieldName: "clubManagement",
            ruName: "Руководство клуба"
        }, {
            fieldName: "medicalStaff",
            ruName: "Мед. персонал"
        }, {
            fieldName: "coach",
            ruName: "Тренер"
        }, {
            fieldName: "forward",
            ruName: "Нападающий"
        }, {
            fieldName: "goalkeeper",
            ruName: "Голкипер"
        }, {
            fieldName: "defender",
            ruName: "Защитник"
        },
    ],
    teamTypes: [{
        fieldName: "less19",
        ruName: "Девушки до 19 лет",
    }, {
        fieldName: "less17",
        ruName: "Девушки до 17 лет",
    }, {
        fieldName: "less15",
        ruName: "Девушки до 15 лет",
    }, {
        fieldName: "teamOfMasters",
        ruName: "Команда мастеров",
    }, {
        fieldName: "nasTeam",
        ruName: "Наицональная сборная",
    }, {
        fieldName: "u19",
        ruName: "Сборная РФ u-19",
    }, {
        fieldName: "u18",
        ruName: "Сборная РФ u-18",
    }, {
        fieldName: "u17",
        ruName: "Сборная РФ u-17",
    }, ],
};