const express = require('express');
const router = express.Router();
const {Models} = require("../models");
const routesCreator = require("../libs/routesCreator");
const logsRouter = require("./logs.route");
const AdmZip = require("adm-zip");
const multer = require("multer");
const path = require("path");


router.use(routesCreator("/news/", Models.news, "admin", ["filter", "getById", {type: "create"}, "deleteById", "deleteMany"]));

router.use(routesCreator("/achievements/", Models.achievements, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));

router.use(routesCreator("/dates/", Models.dates, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));

router.use(routesCreator("/main-slides/", Models.mainSlides, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));

router.use(routesCreator("/mass-media/", Models.massMedia, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));

router.use(routesCreator("/media/", Models.media, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));

router.use(routesCreator("/champion-ships/", Models.championships, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));

router.use(routesCreator("/matches/", Models.matches, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));

router.use(routesCreator("/teams/", Models.teams, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));

router.use(routesCreator("/media-folders/", Models.mediaFolders, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));

router.use(routesCreator("/brand-books/", Models.brandBooks, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));
router.use(routesCreator("/documents/", Models.documents, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));
router.use(routesCreator("/paragraphs/", Models.paragraphs, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));
router.use(routesCreator("/people/", Models.people, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));
router.use(routesCreator("/posters/", Models.posters, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));
router.use(routesCreator("/tours/", Models.tours, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));
router.use(routesCreator("/news-for-fans/", Models.newsForFans, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));
router.use(routesCreator("/partners/", Models.partners, "admin", ["filter", "getById", "create", "deleteById", "deleteMany"]));

// Дальше пускаются только пользователи с ролью admin
router.use(async (req, res, next) => {
    if (req.user.role !== "admin") {
        return res.status(401).send("Вам не хватает прав!");
    }
    next();
});

router.use(routesCreator("/users/", Models.users, "admin", ["filter", "getById", {
    type: "create",
    fields: ["password", "login"]
}, "deleteById", "deleteMany"]));

router.use(logsRouter);
router.use("/logs-files", express.static(path.join(__dirname, "../logs")));

router.post("/builds/", async (req, res, next) => {
    try {
        let upload = multer({
            storage: multer.memoryStorage()
        }).single("file");
        upload(req, res, function (err) {
            if (!req.file) return res.status(422).send("Файл не передан!");
            let buffer = req.file.buffer;
            let zip = new AdmZip(buffer);
            let zipEntries = zip.getEntries();
            let namesArray = zipEntries.flatMap((el) => el.entryName);
            if (namesArray.indexOf('index.html') < 0) return res.status(422).send("Похоже, что вы не передали index.html файл!");
            zipEntries.forEach(function (entry) {
                let entryName = entry.entryName;
                zip.extractEntryTo(entryName, path.join(staticPath, '/public/build/'), true, true);
            });
            res.send("Билд обновлен!");
        });
    } catch (e) {
        console.log(e);
        res.send("Произошла ошибка, похоже архив поврежден!");
    }
});

module.exports = router;