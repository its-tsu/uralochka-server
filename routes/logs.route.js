const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');

const getFiles = function (dir, files_){

    files_ = files_ || [];
    let files = fs.readdirSync(dir);
    for (let i in files){
        let name = dir + '/' + files[i];
        if (fs.statSync(name).isDirectory()){
            getFiles(name, files_);
        } else {
            files_.push(files[i]);
        }
    }
    return files_;
};


/**
  *
  * @api {get} /api/protected/logs GetFiles
 * @apiSampleRequest /api/protected/logs
  * @apiName GetFiles
  * @apiGroup Logs
 * @apiPermission admin
  * @apiVersion  0.0.1
  *
  *
  * @apiSuccessExample {json} Success-Response:
 *     HTTP/ 200 OK
 *{
    "success": true,
    "data": [
        "app.log",
        "error.log"
    ]
}
 *
 */
router.get("/logs/", async function (req, res) {
    try {
        const logList = getFiles(path.join(__dirname, "../logs"));
        res.json({success: true, data: logList});
    } catch (e) {
        console.log(e);
        return res.status(500).json({_message: "Ошибка получения!"});
    }
});


/**
  *
  * @api {get} /api/protected/logs/:name GetFileByName
 * @apiSampleRequest /api/protected/logs/app.log
  * @apiName GetFileByName
  * @apiGroup Logs
 * @apiPermission admin
  * @apiVersion  0.0.1
  *
  *
  * @apiSuccessExample {json} Success-Response:
 *     HTTP/ 200 OK
 *{
    "success": true,
    "data": [
        {
            "date": "2020-06-09T20:30:10.826Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:30:10.828Z",
            "message": "Тестовый запуск ошибки!",
            "level": "error"
        },
        {
            "date": "2020-06-09T20:41:38.713Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:43:46.322Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:44:36.521Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:45:09.097Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:47:31.924Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:48:32.173Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:49:26.171Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:50:18.959Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:52:00.263Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:52:16.932Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:52:42.349Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:53:13.198Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        },
        {
            "date": "2020-06-09T20:53:37.530Z",
            "message": "Сервер запустился на порту: 8080!",
            "level": "info"
        }
    ]
}
 *
 */
router.get("/logs/:file", async function (req, res) {
    try {
        let file = fs.readFileSync(path.join(__dirname, "../logs", req.params.file), {encoding: "utf8"});
        file = JSON.parse(`[${file.split("\n").slice(0, -1).join(",")}]`);
        res.send({success: true, data: file});
    } catch (e) {
        return res.status(500).json({_message: "Ошибка получения!"});
    }
});

module.exports = router;