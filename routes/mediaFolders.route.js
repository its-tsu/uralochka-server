const express = require('express');
const router = express.Router();
const {Models} = require("../models");
const routesCreator = require("../libs/routesCreator");
const {randomInteger} = require("../libs/randomInteger");

/**
  *
  * @api {get} /api/public/media-folders/random RandomMediaFolders
 * @apiSampleRequest /api/public/media-folders/random
 * @apiDescription Возвращает 4 случайных папки в порядке возрастания id.
  * @apiName RandomMediaFolders
  * @apiGroup MediaFolders
 * @apiPermission all
  * @apiVersion  0.0.1
  *
  *
  * @apiSuccessExample {json} Success-Response:
 *     HTTP/ 200 OK
 * {}
 *
 */
router.get("/media-folders/random", async (req, res) => {
    try {
        const dataCount = await Models.mediaFolders.count();
        const offset = randomInteger(0, Math.max(0, dataCount - 5));
        const data = await Models.mediaFolders.findAll({
            limit: 4,
            offset,
            attributes: {exclude: ["deletedAt", "updatedAt", "createdAt"]},
            include: [{
                as: "media",
                model: Models.media,
                limit: 1,
                attributes: {exclude: ["deletedAt", "updatedAt", "createdAt"]},
            }]
        });
        res.send({
            success: true,
            data: data.map(item => DTO(item.get({plain: true}))),
        })
    } catch (e) {
        console.log(e);
        res.send({
            success: false,
        })
    }
});


/**
  *
  * @api {get} /api/public/media-folders Filter
 * @apiSampleRequest /api/public/media-folders
  * @apiName Filter
  * @apiGroup MediaFolders
 * @apiPermission all
  * @apiVersion  0.0.1
  *
  *
 @apiHeader {String} Content-Range Краткое описание того, на какой странице ты находишься

 @apiHeaderExample {json} Response-Example:
 {
    "Content-Range": "`${offset}-${offset+limit}/${entitiesCount}`"
 }
 @apiParam {JSONObject} [filter='{}'] Объект JSON для сортировки.
 @apiParam {JSONArray} [sort='["id", "ASC"]'] Массив JSON, первое поле - по какому полю сортить, второе поле - алгоритм сортировки (DESC или ASC).
 @apiParam {Number{1-100}} [limit=10] Число объектов в ответе.
 @apiParam {Number} [offset=0] Число объектов, которое необходимо пропустить.
 *
  * @apiSuccessExample {json} Success-Response:
 *     HTTP/ 200 OK
 * {}
 *
 */

/**
  *
  * @api {get} /api/public/media-folders/:id GetById
  * @apiSampleRequest /api/public/media-folders/1
  * @apiName GetById
  * @apiGroup MediaFolders
  * @apiPermission all
  * @apiVersion  0.0.1
  *
  *
  * @apiSuccessExample {json} Success-Response:
  *     HTTP/ 200 OK
 * {}
 *
 */
router.use(routesCreator("/media-folders/", Models.mediaFolders, "user",
    [{
        type: "filter",
        include: [{
            as: "media",
            model: Models.media,
            limit: 1
        }],
        DTO,
    }, {
        type: "getById",
        include: [{
            as: "media",
            model: Models.media,
            limit: 1
        }],
        DTO
    }]
));

module.exports = router;

function DTO(folder) {
    folder.imgUrl = folder.media[0] ? folder.media[0].imgUrl : process.env.NO_IMG_URL;
    folder.media = undefined;
    return folder
}