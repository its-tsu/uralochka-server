const express = require('express');
const router = express.Router();
const {Models} = require("../models");
const routesCreator = require("../libs/routesCreator");


/**
  *
  * @api {get} /api/public/championships Filter
 * @apiSampleRequest /api/public/championships
  * @apiName Filter
  * @apiGroup Championships
 * @apiPermission all
  * @apiVersion  0.0.1
  *
  *
 @apiHeader {String} Content-Range Краткое описание того, на какой странице ты находишься

 @apiHeaderExample {json} Response-Example:
 {
    "Content-Range": "`${offset}-${offset+limit}/${entitiesCount}`"
 }
 @apiParam {JSONObject} [filter='{}'] Объект JSON для сортировки.
 @apiParam {JSONArray} [sort='["id", "ASC"]'] Массив JSON, первое поле - по какому полю сортить, второе поле - алгоритм сортировки (DESC или ASC).
 @apiParam {Number{1-100}} [limit=10] Число объектов в ответе.
 @apiParam {Number} [offset=0] Число объектов, которое необходимо пропустить.
 *
  * @apiSuccessExample {json} Success-Response:
 *     HTTP/ 200 OK
 * {}
 *
 */

/**
  *
  * @api {get} /api/public/championships/:id GetById
  * @apiSampleRequest /api/public/championships/1
  * @apiName GetById
  * @apiGroup Championships
  * @apiPermission all
  * @apiVersion  0.0.1
  *
  *
  * @apiSuccessExample {json} Success-Response:
  *     HTTP/ 200 OK
 * {}
 *
 */
router.use(routesCreator("/championships/", Models.championships, "user",
    [{
        type: "filter"
    }, {
        type: "getById",
        include: [
            {
                model: Models.tours,
                as: "tours",
                attributes: {exclude: ["deletedAt", "updatedAt", "createdAt"]},
                include: [
                    {
                        model: Models.matches,
                        as: "matches",
                        attributes: {exclude: ["deletedAt", "updatedAt", "createdAt"]},
                        include: [
                            {
                                model: Models.teams,
                                as: "leftTeam",
                                attributes: {exclude: ["deletedAt", "updatedAt", "createdAt"]},
                            },
                            {
                                model: Models.teams,
                                as: "rightTeam",
                                attributes: {exclude: ["deletedAt", "updatedAt", "createdAt"]},
                            }
                        ]
                    }
                ]
            }
        ]
    }]
));

module.exports = router;
