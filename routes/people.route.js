const express = require('express');
const router = express.Router();
const {Models} = require("../models");
const routesCreator = require("../libs/routesCreator");


/**
  *
  * @api {get} /api/public/people Filter
 * @apiSampleRequest /api/public/people
  * @apiName Filter
  * @apiGroup People
 * @apiPermission all
  * @apiVersion  0.0.1
  *
  *
 @apiHeader {String} Content-Range Краткое описание того, на какой странице ты находишься

 @apiHeaderExample {json} Response-Example:
 {
    "Content-Range": "`${offset}-${offset+limit}/${entitiesCount}`"
 }
 @apiParam {JSONObject} [filter='{}'] Объект JSON для сортировки.
 @apiParam {JSONArray} [sort='["id", "ASC"]'] Массив JSON, первое поле - по какому полю сортить, второе поле - алгоритм сортировки (DESC или ASC).
 @apiParam {Number{1-100}} [limit=10] Число объектов в ответе.
 @apiParam {Number} [offset=0] Число объектов, которое необходимо пропустить.
 *
  * @apiSuccessExample {json} Success-Response:
 *     HTTP/ 200 OK
 * {}
 *
 */

/**
  *
  * @api {get} /api/public/people/:id GetById
  * @apiSampleRequest /api/public/people/1
  * @apiName GetById
  * @apiGroup People
  * @apiPermission all
  * @apiVersion  0.0.1
  *
  *
  * @apiSuccessExample {json} Success-Response:
  *     HTTP/ 200 OK
 * {}
 *
 */
router.use(routesCreator("/people/", Models.people, "user",
    [{
        type: "filter",
        DTO,
    }, {
        type: "getById",
        DTO,
    }]
));

function DTO(item) {
    item.achievements = item.achievements.split("~~");
    return item;
}

module.exports = router;
