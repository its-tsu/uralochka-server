const express = require("express");
const router = express.Router();
const achievementsRouter = require("./achievements.route");
const admin = require("./admin.route");
const brandBooksRouter = require("./brandBooks.route");
const championshipsRouter = require("./championships.route");
const datesRouter = require("./dates.route");
const documentsRouter = require("./documents.route");
const mainSlidesRouter = require("./mainSlides.route");
const massMediaRouter = require("./massMedia.route");
const matchesRouter = require("./matches.route");
const mediaRouter = require("./media.route");
const mediaFoldersRouter = require("./mediaFolders.route");
const newsRouter = require("./news.route");
const newsForFansRouter = require("./newsForFans.route");
const paragraphsRouter = require("./paragraphs.route");
const partnersRouter = require("./partners.route");
const peopleRouter = require("./people.route");
const postersRouter = require("./posters.route");
const teamsRouter = require("./teams.route");
const toursRouter = require("./tours.route");
const usersRouter = require("./users.route");

const tokenCheckerAdmin = require("../libs/tokenCheckerAdmin");

// Роуты пользователей обычных
router.use("/api/public", achievementsRouter);
router.use("/api/public", brandBooksRouter);
router.use("/api/public", championshipsRouter);
router.use("/api/public", datesRouter);
router.use("/api/public", documentsRouter);
router.use("/api/public", mainSlidesRouter);
router.use("/api/public", massMediaRouter);
router.use("/api/public", matchesRouter);
router.use("/api/public", mediaRouter);
router.use("/api/public", mediaFoldersRouter);
router.use("/api/public", newsRouter);
router.use("/api/public", newsForFansRouter);
router.use("/api/public", paragraphsRouter);
router.use("/api/public", partnersRouter);
router.use("/api/public", peopleRouter);
router.use("/api/public", postersRouter);
router.use("/api/public", teamsRouter);
router.use("/api/public", toursRouter);
router.use("/api/public", usersRouter);

// Роуты модераторов

router.use(tokenCheckerAdmin("moderator", "admin"));
router.use("/api/protected", admin);

module.exports = router;



