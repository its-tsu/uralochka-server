const express = require('express');
const router = express.Router();
const {Models} = require("../models");
const jwt = require('jsonwebtoken');
const {log} = require("../libs/log");

/**
  *
  * @api {put} /api/public/users/login Login
 * @apiSampleRequest /api/public/users/login
  * @apiName Login
  * @apiGroup Users
 * @apiPermission all
  * @apiVersion  0.0.1
  *
 @apiErrorExample {json} Error-Response:
 Error 400: Bad Request
 {
    "_message": "Неверный логин или пароль!"
}
  *
 @apiParam {String} password Пароль пользователя.
 @apiParam {String} login Логин пользователя.
 *
  * @apiSuccessExample {json} Success-Response:
 *     HTTP/ 200 OK
 *{
    "success": true,
    "data": {
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTU5MTY0NDgyOSwiZXhwIjoxNTkxNjQ1ODI5fQ.mVfzXlemAIumClFHbZSss8TvKPWMeVZ3jnl2fleppLM",
        "user": {
            "id": 1,
            "login": "admin",
            "password": "admin",
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTU5MTY0NDc4NCwiZXhwIjoxNTkxNjQ1Nzg0fQ.h47ccQYLx1YDLJBzFgBOpT55RfE-_mXNvMsaXx-yXTQ",
            "role": "moderator"
        }
    }
}
 *
 */
router.put("/users/login", async function (req, res) {
    try {
        const user = await Models.users.findOne({where: {login: req.body.login}, attributes: { exclude: ["deletedAt", "updatedAt", "createdAt"] }});
        if(!user || user.password !== req.body.password) {
            return res.status(400).json({_message: "Неверный логин или пароль!"});
        }
        const token = jwt.sign({userId: user.id}, process.env.SECRET, { expiresIn: process.env.TOKEN_LIFE});
        await Models.users.update({token}, {where: {login: req.body.login}});
        res.json({success: true, data: {token, user}});
        try {
            log.info(`Пользователь с id: ${user.id} вошел в систему!`)
        } catch (e) {
            log.error({error: e.toString(), _message: "Ошибка логирования!"});
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({_message: "Неверный логин или пароль!"});
        try {
            //TODO добавить сюда IP адрес
            log.info(`Попытка войти в систему!`);
        } catch (e) {
            log.error({error: e.toString(), _message: "Ошибка логирования!"});
        }
    }
});


module.exports = router;
