(async function () {
    const express = require("express");
    const path = require("path");
    global.staticPath = path.join(__dirname, "static");
    // Подтягиваем переменные для process.env из файла
    require("dotenv").config({ path: path.join(__dirname, "./config/.env") });
    const cors = require("cors");
    const bodyParser = require("body-parser");
    const { log } = require("./libs/log");
    // Синхронизируем БД и модели
    await require("./models").sync();
    const app = express();

    app.use(cors());
    app.use(bodyParser.json({limit: "50mb"}));
    // Подключение статики
    app.use("/admin/api", express.static(path.join(global.staticPath, "public/api")));
    app.use("/admin", express.static(path.join(__dirname, "./admin/build")));
    app.use("/admin/static", express.static(path.join(global.staticPath, "admin")));
    app.use("/public/files", express.static(path.join(global.staticPath, "public/files")));


    const {exec} = require('child_process');
    app.get("/db/export", async (req, res) => {
        try {
            exec('sudo -u postgres pg_dump uralochka > ' + path.join(staticPath, 'admin', 'db', 'backup.sql'), {maxBuffer: 1024 * 1024 * 1024}, err => {
                if (err) {
                    res.send("Произошла ошибка!");
                }
                res.send('Скачай по этому path: /admin/static/db/backup.sql загрузи файлы к себе!');
            });
        } catch (e) {
            console.log(e);
            res.send("Произошла ошибка!");
        }
    });
    app.use("/", express.static("./static/public/build"));

    // Подключение роутов
    app.use(require("./routes"));

    app.listen(process.env.PORT || 8080, function () {
        log.info(`Сервер запустился на порту: ${process.env.PORT}!`);
        console.log(`Сервер запустился на порту: ${process.env.PORT}!`);
    });
})();
