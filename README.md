# uralochka-server

## Управление проектом:

### Для первого запуска проекта необходимо:
- YARN
    - установить БД PostgreSQL:
        - ubuntu:
            - `yarn db:install`
        - windows:
            - https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
    - url для подключения к БД можно поправить в файле .env
    - необходимо запустить следущие команды:
        - `yarn`
        - `cd admin`
        - `yarn`
        - `cd ..`
        - `yarn init-server`
        - `yarn start`

- NPM
    - установить БД Postgres, url для подключения к БД можно поправить в файле .env
    - необходимо запустить следущие команды:
        - `npm i yarn`
        - `./node_modules/yarn/bin/yarn`
        - `cd admin`
        - `../node_modules/yarn/bin/yarn`
        - `cd ..`
        - `npm run init-server`
        - `npm run start`

### Запуск проекта: 
`yarn start`

### Собрать API:
`yarn doc`

### Пересоздание БД (ее дроп!): 
`yarn db-init`

### Сборка админки: 
`yarn admin`

