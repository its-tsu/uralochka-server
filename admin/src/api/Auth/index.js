import {HttpError} from "react-admin";

const authProvider = {
    login: async ({ username, password }) =>  {
        const url = (process.env.REACT_APP_API_URL || "") + "/api/public/users/login";
        let headers = { 'Content-Type': 'application/json;charset=utf-8' };
        let response = await fetch(url, {
            headers,
            method: 'PUT',
            body: JSON.stringify({ login: username, password }),
        });

        if (response.ok) {
            let json = await response.json();
            localStorage.setItem('permissions', json.data.user.role);
            return localStorage.setItem('token', json.data.token);
        } else {
            let json = await response.json();
            throw new HttpError(JSON.stringify(json), response.status, json);
        }
    },
    logout: () => {
        localStorage.removeItem('token');
        localStorage.removeItem('permissions');
        return Promise.resolve();
    },
    checkError: (error) => {
        const status = error.status;
        if (status === 401 || status === 403) {
            localStorage.removeItem('token');
            return Promise.reject();
        }
        return Promise.resolve();
    },
    checkAuth: () => localStorage.getItem('token')
        ? Promise.resolve()
        : Promise.reject({ redirectTo: '/login' }),

    getPermissions: () => {
        const role = localStorage.getItem('permissions');
        return role ? Promise.resolve(role) : Promise.reject();
    }
};

export default authProvider;
