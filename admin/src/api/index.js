// import { fetchUtils } from 'react-admin';
import { stringify } from 'query-string';
import { HttpError } from 'react-admin';
import filesKeys from "../constants/filesKeys";

const apiUrl = (process.env.REACT_APP_API_URL || "") + '/api/protected';
// const httpClient = fetchUtils.fetchJson;

const convertFileToBase64 = file =>
    new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => resolve(reader.result);
        reader.onerror = reject;

        reader.readAsDataURL(file.rawFile);
    });

export default {
    getList: async (resource, params) => {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const query = {
            sort: JSON.stringify([field, order]),
            range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
            filter: JSON.stringify(params.filter),
        };
        const url = `${apiUrl}/${resource}?${stringify(query)}`;
        const token = localStorage.getItem('token');
        let headers = {};
        headers["x-access-token"] = token;
        let response = await fetch(url, {
            headers
        });

        if (response.ok) {
            let total = parseInt(response.headers.get("content-range").split('/').pop(), 10);
            let json = await response.json();
            return {data: json, total};
        } else {
            let json = await response.text();
            throw new HttpError(json, response.status, json);
        }

    },

    getOne: async (resource, params) =>{
        const token = localStorage.getItem('token');
        let headers = {};
        headers["x-access-token"] = token;

        let response = await fetch(`${apiUrl}/${resource}/${params.id}`, {
            headers,
        });

        if (response.ok) {
            let json = await response.json();
            return {data: json};
        } else {
            let json = await response.text();
            throw new HttpError(json, response.status, json);
        }
    },

    getMany: async (resource, params) => {
        const token = localStorage.getItem('token');
        let headers = {};
        headers["x-access-token"] = token;

        const query = {
            filter: JSON.stringify({ id: params.ids }),
        };

        let response = await fetch(`${apiUrl}/${resource}?${stringify(query)}`, {
            headers,
        });

        if (response.ok) {
            let json = await response.json();
            return {data: json};
        } else {
            let json = await response.text();
            throw new HttpError(json, response.status, json);
        }
    },

    getManyReference: async (resource, params) => {
        const token = localStorage.getItem('token');
        let headers = {};
        headers["x-access-token"] = token;

        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const query = {
            sort: JSON.stringify([field, order]),
            range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
            filter: JSON.stringify({
                ...params.filter,
                [params.target]: params.id,
            }),
        };

        let response = await fetch(`${apiUrl}/${resource}?${stringify(query)}`, {
            headers,
        });

        if (response.ok) {
            let total = parseInt(response.headers.get("content-range").split('/').pop(), 10);
            let json = await response.json();
            return {
                data: json,
                total,
            };
        } else {
            let json = await response.text();
            throw new HttpError(json, response.status, json);
        }
    },

    update: async (resource, params) => {
        const token = localStorage.getItem('token');
        let headers = {};
        headers["x-access-token"] = token;
        headers["Content-Type"] = "application/json";

        let response = await fetch(`${apiUrl}/${resource}/${params.id}`, {
            headers,
            method: "PUT",
            body: JSON.stringify(params.data),
        });

        if (response.ok) {
            let json = await response.json();
            return {
                data: json,
            };
        } else {
            let json = await response.text();
            throw new HttpError(json, response.status, json);
        }
    },

    updateMany: async (resource, params) => {
        const token = localStorage.getItem('token');
        let headers = {};
        headers["x-access-token"] = token;
        headers["Content-Type"] = "application/json";

        const query = {
            filter: JSON.stringify({ id: params.ids}),
        };

        let response = await fetch(`${apiUrl}/${resource}?${stringify(query)}`, {
            headers,
            method: "PUT",
            body: JSON.stringify(params.data),
        });

        if (response.ok) {
            let json = await response.json();
            return {
                data: json,
            };
        } else {
            let json = await response.text();
            throw new HttpError(json, response.status, json);
        }
    },

    create: async (resource, params) => {
        const token = localStorage.getItem('token');
        let headers = {'Content-Type': 'application/json;charset=utf-8'};
        headers["x-access-token"] = token;
        headers["Content-Type"] = "application/json";

        let newParams = {
            ...params,
        };
        newParams.data.championships = params.data.championships ? params.data.championships.map(item=>item.text).join("~~") : undefined;
        newParams.data.achievements = params.data.achievements ? params.data.achievements.map(item=>item.text).join("~~") : undefined;

        for (let key of filesKeys){
            if (params.data[key]) {
                if((params.data[key].rawFile instanceof File)) {
                    newParams.data[key] = {src: (await convertFileToBase64(params.data[key])), title: params.data[key].title}
                }
            }
        }

        let response = await fetch(`${apiUrl}/${resource}`, {
            headers,
            body: JSON.stringify(newParams.data),
            method: "POST"
        });

        if (response.ok) {
            let json = await response.json();
            return {data: {...newParams.data, id: json.id}};
        } else {
            let json = await response.text();
            throw new HttpError(json, response.status, json);
        }
    },

    delete: async (resource, params) => {
        const token = localStorage.getItem('token');
        let headers = {};
        headers["x-access-token"] = token;
        headers["Content-Type"] = "application/json";

        let response = await fetch(`${apiUrl}/${resource}/${params.id}`, {
            headers,
            method: "DELETE"
        });

        if (response.ok) {
            let json = await response.json();
            return {data: {...params.data, id: json.id}};
        } else {
            let json = await response.text();
            throw new HttpError(json, response.status, json);
        }
    },

    deleteMany: async (resource, params) => {
        const token = localStorage.getItem('token');
        let headers = {};
        headers["x-access-token"] = token;
        headers["Content-Type"] = "application/json";

        const query = {
            filter: JSON.stringify({ id: params.ids}),
        };

        let response = await fetch(`${apiUrl}/${resource}?${stringify(query)}`, {
            headers,
            method: "DELETE",
            body: JSON.stringify(params.data),
        });

        if (response.ok) {
            let json = await response.json();
            return {
                data: json,
            };
        } else {
            let json = await response.text();
            throw new HttpError(json, response.status, json);
        }
    }
};