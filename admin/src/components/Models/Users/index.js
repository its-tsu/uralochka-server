import {UsersCreate} from "./Create"
import {UsersList} from "./List"

export default {
    create: UsersCreate,
    list: UsersList
}
