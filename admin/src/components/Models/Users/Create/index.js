import {
    ImageField,
    Create,
    ImageInput,
    required,
    SimpleForm,
    TextInput,
} from "react-admin";
import React from "react";


export const UsersCreate = (props) => (
    <Create {...props}>
        <SimpleForm >
            <TextInput fullWidth label="Логин" source="login" validate={required()} />
            <TextInput fullWidth label="Пароль" source="password" validate={required()} />
        </SimpleForm>
    </Create>
);
