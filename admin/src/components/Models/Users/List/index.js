import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton,
} from 'react-admin';

export const UsersList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id"/>
            <TextField label="Логин" source="login"/>
            <TextField label="Пароль" source="password"/>
            <TextField label="Роль" source="role"/>
            <DeleteButton/>
        </Datagrid>
    </List>
};
