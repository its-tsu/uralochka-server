import {BrandBooksCreate} from "./Create"
import {BrandBooksList} from "./List"

export default {
    create: BrandBooksCreate,
    list: BrandBooksList
}
