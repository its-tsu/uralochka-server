import {
    ImageField,
    Create,
    ImageInput,
    required,
    SimpleForm,
    TextInput,NumberInput
} from "react-admin";
import React from "react";


export const BrandBooksCreate = (props) => (
    <Create {...props}>
        <SimpleForm >
            <ImageInput source="img" label="Related pictures" accept="image/*" multiple={false}>
                <ImageField source="img" title="title" />
            </ImageInput>
            <TextInput fullWidth label="Название" source="name" validate={required()} />
        </SimpleForm>
    </Create>
);
