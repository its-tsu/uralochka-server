import {ChampionshipsList} from "./List"
import {ChampionshipsCreate} from "./Create"

export default {
    list: ChampionshipsList,
    create: ChampionshipsCreate,
}