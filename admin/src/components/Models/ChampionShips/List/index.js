import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton,SelectField,
    ImageField, ImageInput, DateField, SelectInput, TextInput, BooleanField, SimpleForm
} from 'react-admin';
import championships from "../../../../constants/championships";

export const ChampionshipsList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id" />
            <ImageField label="Картинка таблицы" source="tableUrl"/>
            <ImageField label="Картинка" source="imgUrl"/>
            <DateField source="date" label="Дата"/>
            <SelectField label="Тип" source="type" choices={championships}/>
            <TextField fullWidth label="Заголовок" source="title"/>
            <TextField fullWidth label="Описание" source="description"/>
            <BooleanField fullWidth label="Отображать на главной странице" source="isActiveOnMainPage"
                          defaultValue={false}/>
            <DeleteButton/>
        </Datagrid>
    </List>};