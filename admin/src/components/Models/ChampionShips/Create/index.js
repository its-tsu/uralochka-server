import {
    ImageField,
    Create,
    ImageInput,
    SimpleForm, TextInput, DateInput, SelectInput, BooleanInput
} from "react-admin";
import React from "react";
import championships from "../../../../constants/championships";


export const ChampionshipsCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <ImageInput source="table" label="Картинка таблицы" accept="image/*" multiple={false}>
                <ImageField source="table" title="title"/>
            </ImageInput>
            <ImageInput source="img" label="Картинка" accept="image/*" multiple={false}>
                <ImageField source="img" title="title"/>
            </ImageInput>
            <DateInput source="date" label="Дата"/>
            <TextInput fullWidth label="Заголовок" source="title"/>
            <TextInput fullWidth label="Описание" source="description"/>
            <SelectInput label="Тип" source="type" choices={championships}/>
            <BooleanInput fullWidth label="Отображать на главной странице" source="isActiveOnMainPage"
                          defaultValue={false}/>
        </SimpleForm>
    </Create>
);