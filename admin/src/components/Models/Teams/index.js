import {TeamsList} from "./List"
import {TeamsCreate} from "./Create"

export default {
    list: TeamsList,
    create: TeamsCreate,
}