import {
    ImageField,
    Create,
    ImageInput,
    SimpleForm,
    TextInput,
} from "react-admin";
import React from "react";


export const TeamsCreate = (props) => (
    <Create {...props}>
        <SimpleForm >
            <TextInput fullWidth label="Название" source="name"/>
            <ImageInput source="img" label="Картинка" accept="image/*" multiple={false}>
                <ImageField source="img" title="title" />
            </ImageInput>
        </SimpleForm>
    </Create>
);