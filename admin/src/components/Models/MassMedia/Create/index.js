import {
    ImageField,
    Create,
    ImageInput,
    SimpleForm,
    TextInput, DateInput,
} from "react-admin";
import React from "react";


export const MassMediaCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput fullWidth label="Заголовок" source="title"/>
            <TextInput fullWidth label="Текст" source="text"/>
            <TextInput fullWidth label="Ссылка" source="link"/>
            <ImageInput source="img" label="Картинка" accept="image/*" multiple={false}>
                <ImageField source="img" title="title" />
            </ImageInput>
            <DateInput  label="Дата" source="date" defaultValue={new Date()}/>
        </SimpleForm>
    </Create>
);
