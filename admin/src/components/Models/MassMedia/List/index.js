import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton, DateField,ImageField
} from 'react-admin';

export const MassMediaList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id" />
            <TextField label="Заголовок" source="title"/>
            <TextField label="Текст" source="text"/>
            <TextField label="Ссылка" source="link"/>
            <ImageField label="Картинка" source="imgUrl"/>
            <DateField label="Дата" source="date"/>
            <DeleteButton/>
        </Datagrid>
    </List>};
