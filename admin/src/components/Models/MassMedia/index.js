import {MassMediaList} from "./List"
import {MassMediaCreate} from "./Create"

export default {
    list: MassMediaList,
    create: MassMediaCreate,
}