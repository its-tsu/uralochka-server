import React from 'react';
import {
    List,
    Datagrid,
    TextField,ImageField,
    DeleteButton, DateField, SelectField, ReferenceField,
} from 'react-admin';
import media from "../../../../constants/media";

export const MediaList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id" />
            <TextField label="Название" source="name"/>
            <TextField label="Ссылка на видео" source="link"/>
            <SelectField  label="Тип" source="type" choices={media} />
            <DateField label="Дата" source="date"/>
            <ImageField label="Картинка/Превью" source="imgUrl"/>
            <ReferenceField label="Папка с картинками" source="mediaFolderId" reference="media-folders">
                <TextField source="name"/>
            </ReferenceField>
            <DeleteButton/>
        </Datagrid>
    </List>};
