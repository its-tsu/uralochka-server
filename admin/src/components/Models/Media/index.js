import {MediaList} from "./List"
import {MediaCreate} from "./Create"

export default {
    list: MediaList,
    create: MediaCreate,
}