import {
    ImageField,
    Create,
    ImageInput,
    SimpleForm,
    TextInput, DateInput, SelectInput, ReferenceInput, FileInput,FileField
} from "react-admin";
import React from "react";
import media from "../../../../constants/media";


export const MediaCreate = (props) => (
    <Create {...props}>
        <SimpleForm >
            <TextInput fullWidth label="Название" source="name" />
            <SelectInput  label="Тип" source="type" choices={media} />
            <DateInput  label="Дата" source="date" defaultValue={new Date()}/>
            <ImageInput source="img" label="Картинка/Превью" accept="image/*" multiple={false}>
                <ImageField source="img" title="title" />
            </ImageInput>
            <TextInput fullWidth label="Ссылка на видео" source="link" />
            <ReferenceInput  label="Папка с картинками" source="mediaFolderId" reference="media-folders">
                <SelectInput fullWidth optionText="name"/>
            </ReferenceInput>
        </SimpleForm>
    </Create>
);
