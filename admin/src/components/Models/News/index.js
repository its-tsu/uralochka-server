import {NewsList} from "./List"
import {NewsCreate} from "./Create"

export default {
    list: NewsList,
    create: NewsCreate,
}