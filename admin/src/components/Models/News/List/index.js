import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DateField,
    DeleteButton, SelectField, ImageField
} from 'react-admin';
import news from "../../../../constants/news";

export const NewsList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id"/>
            <TextField label="Заголовок" source="title"/>
            <SelectField  label="Тип" source="type" choices={news}/>
            <TextField label="Текст" source="text"/>
            <DateField label="Дата" source="date"/>
            <ImageField label="Картинка" source="imgUrl"/>
            <DeleteButton/>
        </Datagrid>
    </List>
};