import {
    ImageField,
    Create,
    ImageInput,
    SimpleForm,
    TextInput,
    DateInput, SelectInput
} from "react-admin";
import React from "react";
import news from "../../../../constants/news";


export const NewsCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput fullWidth label="Заголовок" source="title"/>
            <SelectInput  label="Тип" source="type" choices={news}/>
            <TextInput fullWidth label="Текст" source="text"/>
            <DateInput  label="Дата" source="date" defaultValue={new Date()}/>
            <ImageInput source="img" label="Картинка" accept="image/*" multiple={false}>
                <ImageField source="img" title="title" />
            </ImageInput>
        </SimpleForm>
    </Create>
);
