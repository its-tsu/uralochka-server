import {DocumentsCreate} from "./Create"
import {DocumentsList} from "./List"

export default {
    create: DocumentsCreate,
    list: DocumentsList
}
