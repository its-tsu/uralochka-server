import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton, required, SelectField, FileField
} from 'react-admin';
import documents from "../../../../constants/documents";

export const DocumentsList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id"/>
            <TextField label="Название" source="name"/>
            <SelectField  label="Тип" source="type" choices={documents}  validate={required()} />
            <FileField label="Файл" source="fileUrl"/>
            <DeleteButton/>
        </Datagrid>
    </List>
};
