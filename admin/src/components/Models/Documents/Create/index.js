import {
    FileField,
    Create,
    ImageInput,
    SimpleForm,
    TextInput, SelectInput
} from "react-admin";
import React from "react";
import documents from "../../../../constants/documents";


export const DocumentsCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput fullWidth label="Название" source="name"/>
            <SelectInput  label="Тип" source="type" choices={documents}/>
            <ImageInput source="file" label="Файл" multiple={false}>
                <FileField source="file" title="title" />
            </ImageInput>
        </SimpleForm>
    </Create>
);
