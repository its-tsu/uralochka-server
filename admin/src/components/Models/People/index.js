import {PeopleList} from "./List"
import {PeopleCreate} from "./Create"

export default {
    list: PeopleList,
    create: PeopleCreate,
}