import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton, BooleanField, ImageField, required, SelectField
} from 'react-admin';
import {teamTypes, types} from "../../../../constants/people";

const Achievements = (props) => {
    return props.record.achievements.split("~~").map((item, index) => {
        return index !== 0 ? `\n${item}` : item
    })
}
Achievements.defaultProps = {label: "Достижения"};

export const PeopleList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id" />
            <ImageField label="Картинка" source="imgUrl"/>
            <ImageField label="Вторая картинка" source="secondImgUrl"/>
            <TextField label="Имя" source="firstName"/>
            <TextField label="Отчество" source="middleName"/>
            <TextField label="Фамилия" source="surname"/>
            {/*<TextField label="Первый текст" source="firstParagraph"/>*/}
            {/*<TextField label="Второй текст" source="secondParagraph"/>*/}
            <TextField label="Должность" source="position"/>
            <SelectField fullWidth label="Тип" source="type" validate={required()} choices={types}/>
            <SelectField fullWidth label="Тип команды" source="teamType" validate={required()} choices={teamTypes}/>
            {/*<TextField label="Достижения" source="achievements"/>*/}
            <Achievements/>
            <BooleanField label="Отображать тренера на странице команды" source="isViewOnTeamPage"/>
            <DeleteButton/>
        </Datagrid>
    </List>
};