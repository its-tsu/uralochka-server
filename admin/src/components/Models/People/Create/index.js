import {
    ImageField,
    Create,
    ImageInput,
    required,
    SimpleForm,
    TextInput, BooleanInput, SelectInput, ArrayInput, SimpleFormIterator
} from "react-admin";
import React from "react";
import {teamTypes, types} from "../../../../constants/people";


export const PeopleCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <ImageInput source="img" label="Первая картинка" accept="image/*" multiple={false}>
                <ImageField source="img" title="title" />
            </ImageInput>
            <ImageInput source="secondImg" label="Вторая картинка" accept="image/*" multiple={false}>
                <ImageField source="secondImg" title="title" />
            </ImageInput>
            <TextInput fullWidth label="Имя" source="firstName" validate={required()} />
            <TextInput fullWidth label="Отчество" source="middleName" />
            <TextInput fullWidth label="Фамилия" source="surname"  />
            <TextInput fullWidth label="Текст" source="firstParagraph"  />
            <TextInput fullWidth label="Текст" source="secondParagraph"  />
            <TextInput fullWidth label="Должность" source="position"  />
            <SelectInput fullWidth label="Тип" source="type" validate={required()} choices={types}/>
            <SelectInput fullWidth label="Тип команды" source="teamType" validate={required()} choices={teamTypes}/>
            <ArrayInput label="Достижения" source="achievements">
                <SimpleFormIterator>
                    <TextInput source="text" label="Достижение"/>
                </SimpleFormIterator>
            </ArrayInput>
            <BooleanInput fullWidth label="Отображать тренера на странице команды" source="isViewOnTeamPage" defaultValue={false} />
        </SimpleForm>
    </Create>
);
