import {
    Create,
    required,
    SimpleForm,
    TextInput, SelectInput
} from "react-admin";
import React from "react";
import {positions, types} from "../../../../constants/paragraphs";


export const ParagraphsCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput fullWidth label="Текст" source="text" validate={required()} />
            <SelectInput  label="Тип" source="type" choices={types}  validate={required()} />
            <SelectInput  label="Позиция" source="position" choices={positions}  validate={required()} />
        </SimpleForm>
    </Create>
);
