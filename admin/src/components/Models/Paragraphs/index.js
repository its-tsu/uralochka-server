import {ParagraphsList} from "./List"
import {ParagraphsCreate} from "./Create"

export default {
    list: ParagraphsList,
    create: ParagraphsCreate,
}