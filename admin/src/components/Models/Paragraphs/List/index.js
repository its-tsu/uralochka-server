import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton, SelectField, required
} from 'react-admin';
import {positions, types} from "../../../../constants/paragraphs";

export const ParagraphsList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id"/>
            <TextField label="Текст" source="text"/>
            <SelectField  label="Тип" source="type" choices={types}  validate={required()} />
            <SelectField  label="Позиция" source="position" choices={positions}  validate={required()} />
            <DeleteButton/>
        </Datagrid>
    </List>
};