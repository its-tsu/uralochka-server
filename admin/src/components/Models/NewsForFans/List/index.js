import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DateField,
    DeleteButton,ImageField
} from 'react-admin';

export const NewsForFansList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id"/>
            <TextField label="Заголовок" source="title"/>
            <TextField label="Текст" source="text"/>
            <ImageField label="Картинка" source="imgUrl"/>
            <DateField label="Дата" source="date"/>
            <DeleteButton/>
        </Datagrid>
    </List>
};