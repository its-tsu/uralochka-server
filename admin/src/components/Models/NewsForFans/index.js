import {NewsForFansList} from "./List"
import {NewsForFansCreate} from "./Create"

export default {
    list: NewsForFansList,
    create: NewsForFansCreate,
}