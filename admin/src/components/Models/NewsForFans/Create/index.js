import {
    ImageField,
    Create,
    ImageInput,
    required,
    SimpleForm,
    TextInput,
    DateInput
} from "react-admin";
import React from "react";


export const NewsForFansCreate = (props) => (
    <Create {...props}>
        <SimpleForm >
            <TextInput fullWidth label="Заголовок" source="title" validate={required()} />
            <TextInput fullWidth label="Текст" source="text" validate={required()} />
            <ImageInput source="img" label="Related pictures" accept="image/*" multiple={false}>
                <ImageField source="img" title="title" />
            </ImageInput>
            <DateInput  label="Дата" source="date" defaultValue={new Date()}/>
        </SimpleForm>
    </Create>
);
