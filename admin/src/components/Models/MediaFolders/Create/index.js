import {
    Create,
    required,
    SimpleForm,
    TextInput,DateInput
} from "react-admin";
import React from "react";


export const MediaFoldersCreate = (props) => (
    <Create {...props}>
        <SimpleForm >
            <TextInput fullWidth label="Название" source="name" validate={required()} />
            <DateInput fullWidth label="Дата" source="date" validate={required()} />
        </SimpleForm>
    </Create>
);
