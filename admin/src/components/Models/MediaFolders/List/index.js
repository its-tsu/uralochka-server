import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton,DateField
} from 'react-admin';

export const MediaFoldersList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id"/>
            <TextField label="Название" source="name"/>
            <DateField label="Дата" source="date"/>
            <DeleteButton/>
        </Datagrid>
    </List>
};
