import {MediaFoldersCreate} from "./Create"
import {MediaFoldersList} from "./List"

export default {
    create: MediaFoldersCreate,
    list: MediaFoldersList
}
