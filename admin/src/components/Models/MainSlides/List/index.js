import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton, ImageField,
} from 'react-admin';

export const MainSlidesList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id" />
            <TextField label="Название" source="name"/>
            <ImageField label="Картинка" source="imgUrl"/>
            <DeleteButton/>
        </Datagrid>
    </List>};
