import {MainSlidesList} from "./List"
import {MainSlidesCreate} from "./Create"

export default {
    list: MainSlidesList,
    create: MainSlidesCreate,
}