import {
    ImageField,
    Create,
    ImageInput,
    required,
    SimpleForm,
    TextInput, DateInput,
} from "react-admin";
import React from "react";


export const DatesCreate = (props) => (
    <Create {...props}>
        <SimpleForm >
            <TextInput fullWidth label="Описание" source="description" />
            <ImageInput source="img" label="Картинка" accept="image/*" multiple={false}>
                <ImageField source="img" title="title" />
            </ImageInput>
            <DateInput  label="Дата" source="date" defaultValue={new Date()}/>
        </SimpleForm>
    </Create>
);