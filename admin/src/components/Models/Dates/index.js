import {DatesList} from "./List"
import {DatesCreate} from "./Create"

export default {
    list: DatesList,
    create: DatesCreate,
}