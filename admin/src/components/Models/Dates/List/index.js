import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DateField,
    DeleteButton, ImageField
} from 'react-admin';

export const DatesList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id"/>
            <TextField label="Описание" source="description"/>
            <ImageField label="Картинка" source="imgUrl"/>
            <DateField label="Дата" source="date"/>
            <DeleteButton/>
        </Datagrid>
    </List>
};