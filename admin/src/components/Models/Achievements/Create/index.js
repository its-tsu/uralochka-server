import {
    ImageField,
    Create,
    ImageInput,
    SimpleForm,SimpleFormIterator,
    TextInput,NumberInput, ArrayInput
} from "react-admin";
import React from "react";


export const AchievementsCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <ImageInput source="secondImg" label="Вторая картинка" accept="image/*" multiple={false}>
                <ImageField source="secondImg" title="title" />
            </ImageInput>
            <TextInput fullWidth label="Название" source="name"/>
            <NumberInput source="position" label="Место"/>
            <TextInput fullWidth label="Первый абзац" source="firstParagraph" />
            <TextInput fullWidth label="Второй абзац" source="secondParagraph" />
            <ArrayInput label="Соревнования" source="championships">
                <SimpleFormIterator>
                    <TextInput source="text"  label="Соревнование"/>
                </SimpleFormIterator>
            </ArrayInput>
        </SimpleForm>
    </Create>
);
