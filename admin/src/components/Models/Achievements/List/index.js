import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton, NumberField,ImageField
} from 'react-admin';

const Championships = (props) => {
    return props.record.championships.split("~~").map((item, index) => {
        return index !== 0 ? `\n${item}` : item
    })
}
Championships.defaultProps = {label: "Соревнования"};

export const AchievementsList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id"/>
            <ImageField label="Картинка" source="secondImgUrl"/>
            <TextField label="Название" source="name"/>
            <NumberField source="position" label="Место"/>
            {/*<TextField label="Первый абзац" source="firstParagraph"/>*/}
            {/*<TextField label="Второй абзац" source="secondParagraph"/>*/}
            <Championships/>
            <DeleteButton/>
        </Datagrid>
    </List>
};
