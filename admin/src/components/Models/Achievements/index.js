import {AchievementsCreate} from "./Create"
import {AchievementsList} from "./List"

export default {
    create: AchievementsCreate,
    list: AchievementsList
}
