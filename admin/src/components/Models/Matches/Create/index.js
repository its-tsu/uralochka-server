import {
    Create, DateInput,
    required,
    SimpleForm, NumberInput, ReferenceInput, SelectInput, TextInput, ImageInput, ImageField, DateTimeInput
} from "react-admin";
import React from "react";
import Grid from "@material-ui/core/Grid";


export const MatchesCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput fullWidth label="Название" source="name"/>
            <TextInput fullWidth label="Описание" source="description"/>
            <ImageInput source="protocolImg" label="Картинка" accept="image/*" multiple={false}>
                <ImageField source="protocolImg" title="title" />
            </ImageInput>
            {/*<DateInput label="Дата" source="date" defaultValue={new Date()}/>*/}
            <DateTimeInput source="date" label="Дата" />
            <TextInput fullWidth label="Ссылка на трансляцию" source="link"/>
            <Grid fullWidth container spacing={3}>
                <Grid item xs={6}>
                    <ReferenceInput  label="Левая команда" source="leftTeamId" reference="teams">
                        <SelectInput fullWidth optionText="name"/>
                    </ReferenceInput>
                    <NumberInput fullWidth source="leftPoints" label="Очки левой команды"/>
                </Grid>
                <Grid item xs={6}>
                    <ReferenceInput label="Правая команда" source="rightTeamId" reference="teams">
                        <SelectInput fullWidth optionText="name"/>
                    </ReferenceInput>
                <NumberInput fullWidth source="rightPoints" label="Очки правой команды"/>
                </Grid>
            </Grid>

            <ReferenceInput  label="Тур" source="tourId" reference="tours">
                <SelectInput fullWidth optionText="name"/>
            </ReferenceInput>

            <ReferenceInput  label="Папка с картинками" source="mediaFolderId" reference="media-folders">
                <SelectInput fullWidth optionText="name"/>
            </ReferenceInput>

           </SimpleForm>
    </Create>
);
