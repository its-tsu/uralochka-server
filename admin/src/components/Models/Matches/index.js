import {MatchesList} from "./List"
import {MatchesCreate} from "./Create"

export default {
    list: MatchesList,
    create: MatchesCreate,
}