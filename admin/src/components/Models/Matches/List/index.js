import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton, NumberField, ReferenceField, DateField, ImageField
} from 'react-admin';

export const MatchesList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id"/>

            <TextField label="Название" source="name"/>
            <TextField label="Описание" source="description"/>
            <ImageField label="Картинка" source="protocolImgUrl"/>
            <DateField label="Дата" source="date"/>
            <TextField label="Ссылка на трансляцию" source="link"/>


            <NumberField source="leftPoints" label="Очки левой команды"/>
            <ReferenceField label="Левая команда" source="leftTeamId" reference="teams">
                <TextField source="name"/>
            </ReferenceField>


            <NumberField source="rightPoints" label="Очки правой команды"/>
            <ReferenceField label="Правая команда" source="rightTeamId" reference="teams">
                <TextField source="name"/>
            </ReferenceField>

            <ReferenceField label="Тур" source="tourId" reference="tours">
                <TextField source="name"/>
            </ReferenceField>

            <ReferenceField label="Папка с картинками" source="mediaFolderId" reference="media-folders">
                <TextField source="name"/>
            </ReferenceField>

            <DeleteButton/>
        </Datagrid>
    </List>
};