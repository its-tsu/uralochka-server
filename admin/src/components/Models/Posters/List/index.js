import React from 'react';
import {
    List,
    Datagrid,
    TextField,ImageField,
    DeleteButton, SelectField,
} from 'react-admin';
import {types} from "../../../../constants/posters";

export const PostersList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id" />
            <TextField label="Название" source="name"/>
            <SelectField fullWidth label="Тип" source="type" choices={types}/>
            <ImageField label="Картинка" source="imgUrl"/>
            <DeleteButton/>
        </Datagrid>
    </List>};
