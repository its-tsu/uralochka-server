import {PostersList} from "./List"
import {PostersCreate} from "./Create"

export default {
    list: PostersList,
    create: PostersCreate,
}