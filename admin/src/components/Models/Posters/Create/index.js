import {
    ImageField,
    Create,
    ImageInput,
    required,
    SimpleForm,
    TextInput,SelectInput
} from "react-admin";
import React from "react";
import {types} from "../../../../constants/posters";


export const PostersCreate = (props) => (
    <Create {...props}>
        <SimpleForm >
            <TextInput fullWidth label="Название" source="name" />
            <SelectInput fullWidth label="Тип" source="type" choices={types}/>
            <ImageInput source="img" label="Картинка" accept="image/*" multiple={false}>
                <ImageField source="img" title="title" />
            </ImageInput>
        </SimpleForm>
    </Create>
);
