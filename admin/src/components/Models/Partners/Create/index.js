import {
    ImageField,
    Create,
    ImageInput,
    required,
    SimpleForm,
    TextInput, SelectInput,
} from "react-admin";
import React from "react";
import partners from "../../../../constants/partners";


export const PartnersCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput fullWidth label="Название" source="name" validate={required()} />
            <TextInput fullWidth label="Ссылка" source="link" validate={required()} />
            <SelectInput  label="Тип" source="type" choices={partners}/>
            <ImageInput source="img" label="Related pictures" accept="image/*" multiple={false}>
                <ImageField source="img" title="title" />
            </ImageInput>
        </SimpleForm>
    </Create>
);