import {PartnersList} from "./List"
import {PartnersCreate} from "./Create"

export default {
    list: PartnersList,
    create: PartnersCreate,
}