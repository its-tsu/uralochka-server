import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DeleteButton, SelectField, ImageField,
} from 'react-admin';
import partners from "../../../../constants/partners";

export const PartnersList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id" />
            <TextField label="Название" source="name"/>
            <TextField label="Ссылка" source="link"/>
            <ImageField label="Картинка" source="imgUrl"/>
            <SelectField  label="Тип" source="type" choices={partners}/>
            <DeleteButton/>
        </Datagrid>
    </List>};