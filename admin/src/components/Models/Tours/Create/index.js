import {
    SelectInput,
    Create,
    ReferenceInput,
    required,
    SimpleForm,
    TextInput,
    DateInput
} from "react-admin";
import React from "react";


export const ToursCreate = (props) => (
    <Create {...props}>
        <SimpleForm >
            <TextInput fullWidth label="Название" source="name" />
            {/*<DateInput fullWidth label="Дата" source="date" />*/}
            <TextInput fullWidth label="Заголвок" source="title" />
            <ReferenceInput fullWidth label="Соревнование" source="championshipId" validate={required()}  reference="champion-ships">
                <SelectInput optionText="title" />
            </ReferenceInput>
            <TextInput fullWidth label="Место" source="location" validate={required()} />
        </SimpleForm>
    </Create>
);
