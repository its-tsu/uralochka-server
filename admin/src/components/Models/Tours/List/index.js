import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DateField,
    DeleteButton
} from 'react-admin';

export const ToursList = props => {
    const {options} = props;
    const {label} = options;
    return <List {...props} exporter={false} title={label}>
        <Datagrid>
            <TextField label="id" source="id"/>
            <TextField label="Название" source="name"/>
            {/*<DateField label="Дата" source="date"/>*/}
            <TextField label="Заголовок" source="title"/>
            <TextField label="Соревнование" source="championshipId"/>
            <TextField label="Место" source="location"/>
            <DeleteButton/>
        </Datagrid>
    </List>
};
