import {ToursList} from "./List"
import {ToursCreate} from "./Create"

export default {
    list: ToursList,
    create: ToursCreate,
}