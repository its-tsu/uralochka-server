import React, {Component} from "react";
import Files from "./Files";
import Table from "./Table";
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import CardContent from '@material-ui/core/CardContent';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Link from '@material-ui/core/Link';

class Logs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            files: [],
            current: -1,
            color: false
        };
    }

    setFiles = (files) => {
        this.setState({files: files});
    };

    setFile = (file) => {
        this.setState({current: file});
    };

    handleChange = (event) => {
        this.setState({color: event.target.checked});
    };

    render() {
        const {files, current, color} = this.state;
        return <>
            <Grid container spacing={3}>
                <Grid item xs={8}>
                    <Card>
                        <CardContent>
                            <Files setFiles={this.setFiles} setFile={this.setFile} files={files}/>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={4}>
                    <Card>
                        <CardContent>
                            <FormControlLabel
                                control={ <Checkbox
                                    color="secondary"
                                    onChange={this.handleChange}
                                    inputProps={{'aria-label': 'secondary checkbox'}}
                                />}
                                label="Цветовая подсветка"
                            />
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12}>
                    <Card>
                        <Table color={color} current={current}/>
                    </Card>
                </Grid>
            </Grid>
        </>
    }
}

export default Logs;
