import React, {Component} from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import moment from "moment";
import Loading from "../../Loading";

const style = {
    default: {
        panel:{
            background: "rgba(255,255,255,0.2)",
        },
    },
    error: {
        panel:{
            background: "rgba(250,0,42,0.2)",
        },
    },
    warn: {
        panel: {
            background: "rgba(254,212,27,0.2)"
        }
    },
    info: {
        panel: {
            background: "rgba(102,255,0,0.2)"
        }
    },
    http: {
        panel: {
            background: "rgba(255,129,5,0.2)"
        }
    },
    verbose: {
        panel: {
            background: "rgba(27,199,254,0.2)"
        }
    },
    debug: {
        panel: {
            background: "rgba(117,117,117,0.2)"
        }
    },
    silly: {
        panel: {
            background: "rgba(251,27,254,0.2)"
        }
    }
};

class Table extends Component {

    constructor(props){
        super(props);
        this.state = {
            logs: [],
            isLoaded: true,
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {current} = this.props;
        if (prevProps.current !== current) {
            this.getLogs();
        }
    }

    getLogs = async () => {
        this.setState({isLoaded: false});
        const apiUrl = (process.env.REACT_APP_API_URL || "") + `/api/protected`;
        const token = localStorage.getItem('token');
        let headers = {};
        headers["x-access-token"] = token;
        let response = await fetch(`${apiUrl}/logs/${this.props.current}`, {headers});
        if (response.ok) {
            const logs = await response.json();
            this.setState({logs: logs.data});
        }
        this.setState({isLoaded:true});
    };

    render() {
        const {logs, isLoaded} = this.state;
        const {color} = this.props;
        const noColor = color ? null : "default";
        return <Loading isLoaded={isLoaded}>
            {
                logs.map((log, index) => {
                    return <ExpansionPanel style={{...style[noColor || log.level].panel, margin:0, transition: `background ${.4 + index/100}s`}} disabled={!log.data}>
                            <ExpansionPanelSummary
                                style={{opacity:1}}
                                expandIcon={log.data && <ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography style={{flexBasis: '20%', fontSize: "14px"}}>{moment(log.date).format("DD.MM.YYYY hh:mm:ss")}</Typography>
                                <Typography  style={{flexBasis: '80%', fontSize: "14px"}}>{log?.message || log?.data?._message}</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                                <Typography>
                                   <pre>
                                       {JSON.stringify(log,null, 2)}
                                   </pre>
                                </Typography>
                            </ExpansionPanelDetails>
                        </ExpansionPanel>
                })
            }
        </Loading>
    }
}

export default Table;
