import React, {Component} from 'react';
import Loading from "../../Loading";
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

class Files extends Component{

    constructor(props){
        super(props);
        this.state = {
            isLoaded: true
        }
    }

    componentDidMount() {
       this.getFiles();
    };

    getFiles = async () => {
        this.setState({isLoaded: false});
        const apiUrl = (process.env.REACT_APP_API_URL || "") + '/api/protected';
        const token = localStorage.getItem('token');
        let headers = {};
        headers["x-access-token"] = token;
        let response = await fetch(`${apiUrl}/logs`, {headers});
        if (response.ok){
            const files = await response.json();
            this.props.setFiles(files.data);
        }
        this.setState({isLoaded: true});
    };

    setFile = (file) => () => {
        this.props.setFile(file);
    };

    render() {
        const {files} = this.props;
        const {isLoaded} = this.state;
        return <Loading isLoaded={isLoaded}>
            {
                files?.map(file => {
                    return  <ButtonGroup style={{width:"100%"}} variant="text" color="primary" aria-label="text primary button group">
                        <Button style={{flexGrow: 1}} onClick={this.setFile(file)} key={file}>{file}</Button>
                        <Button><a style={{textDecoration:"none"}} href="#">Скачать файл</a></Button>
                    </ButtonGroup>
                })
            }
        </Loading>
    }
}

export default Files;
