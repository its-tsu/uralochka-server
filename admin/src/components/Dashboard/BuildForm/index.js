import React from 'react';
import Button from '@material-ui/core/Button';

const apiUrl = (process.env.REACT_APP_API_URL || "") + '/api/protected';

export default () => {
    return (
        <form onSubmit={async e => {
            e.preventDefault();
            const data = new FormData(e.target);
            const token = localStorage.getItem('token');
            let headers = {};
            headers["x-access-token"] = token;

            let response = await fetch(`${apiUrl}/builds`, {
                headers,
                body: data,
                method: "POST"
            });
            alert(await response.text());
        }}>
            <input name="file" type="file"/>
            <Button variant="contained" color="primary" type="submit">Отправить</Button>
        </form>
    )
};
