import React, {Component} from "react";
import LinearProgress from '@material-ui/core/LinearProgress';

class Loading extends Component{
    render() {
        const {isLoaded, children} = this.props;
        return isLoaded ? children : <LinearProgress color="secondary" />
    }
}

export default Loading;
