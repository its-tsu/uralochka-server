import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import {Title} from 'react-admin';
import Logs from "./Logs";
import BuildForm from "./BuildForm";
import Grid from '@material-ui/core/Grid';

export default (props) => {
    return (
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <Card>
                    <Title title="Тут будет что-то типа инструкции"/>
                    <CardContent> Надо написать краткий экскурс! <a href="/admin/api">API</a> </CardContent>
                </Card>
            </Grid>
            {props.permissions === "admin" && <Grid item xs={12}><BuildForm/></Grid>}
            {props.permissions === "admin" && <Grid item xs={12}><Logs/></Grid>}
        </Grid>
    )
};
