import {TabbedShowLayout, TextField, Tab, DateField, EditButton, ReferenceManyField, Datagrid, BooleanField, Show, RichTextField } from "react-admin";
import React from "react";

export const UserShow = (props) => (
    <Show {...props}>
        <TabbedShowLayout>
            <Tab label="Пользователь">
                <TextField label="Id" source="id" />
                <TextField source="login" />
                <TextField source="password" />
            </Tab>
            <Tab label="Токены" path="tokens">
                <ReferenceManyField reference="tokens" target="userId" addLabel={false}>
                    <Datagrid>
                        <TextField source="id" />
                        <RichTextField  source="token" stripTags />
                        <BooleanField source="isValid" />
                        <DateField source="createdAt" showTime />
                        <EditButton />
                    </Datagrid>
                </ReferenceManyField>
            </Tab>
        </TabbedShowLayout>
    </Show>
);