import {AutocompleteInput, Edit, ReferenceInput, required, SimpleForm, TextInput} from "react-admin";
import React from "react";

export const UserEdit = (props) => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput disabled label="Id" source="id" />
            <TextInput label="Логин" source="login" validate={required()} />
            <TextInput label="Пароль" source="password" validate={required()} />
            <ReferenceInput label="Роль" source="roleId" reference="roles" >
                <AutocompleteInput optionText="name"/>
            </ReferenceInput>
        </SimpleForm>
    </Edit>
);