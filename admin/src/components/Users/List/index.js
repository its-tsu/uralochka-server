import React from 'react';
import {
    EditButton,
    Filter,
    TextInput,
    List,
    Datagrid,
    TextField,
    ReferenceField,
    ShowButton,
    ReferenceInput,
    AutocompleteInput,
} from 'react-admin';

const PostFilter = (props) => (
    <Filter {...props}>
        <TextInput label="id" source="id" />
        <TextInput label="логин" source="login" alwaysOn/>
        <ReferenceInput label="Роль" source="roleId" reference="roles" >
            <AutocompleteInput optionText="name"/>
        </ReferenceInput>
    </Filter>
);

export const UserList = props => (
    <List filters={<PostFilter />} {...props}>
        <Datagrid isRowSelectable={ () => false } rowClick="edit">
            <TextField label="id" source="id" />
            <TextField label="Логин" source="login" />
            <TextField label="Пароль" source="password" />
            <ReferenceField label="Роль" reference="roles" source="roleId">
                <TextField source="name" />
            </ReferenceField>
            <EditButton />
            <ShowButton />
        </Datagrid>
    </List>
);