import {AutocompleteInput, Create, ReferenceInput, required, SimpleForm, TextInput} from "react-admin";
import React from "react";


export const UserCreate = (props) => {
    return(
        <Create {...props}>
            <SimpleForm invalid={false}>
                <TextInput label="Логин" source="login" validate={required()} />
                <TextInput label="Пароль" source="password" validate={required()} />
                <ReferenceInput label="Роль" source="roleId" reference="roles" >
                    <AutocompleteInput optionText="name"/>
                </ReferenceInput>
            </SimpleForm>
        </Create>
    );
}