export const types = [
    {
        id: "hallOfFame",
        name: "Зал славы"
    }, {
        id: "president",
        name: "Президент"
    }, {
        id: "boardOfDirectors",
        name: "Совет директоров"
    }, {
        id: "clubManagement",
        name: "Руководство клуба"
    }, {
        id: "medicalStaff",
        name: "Мед. персонал"
    }, {
        id: "coach",
        name: "Тренер"
    }, {
        id: "forward",
        name: "Нападающий"
    }, {
        id: "goalkeeper",
        name: "Голкипер"
    }, {
        id: "defender",
        name: "Защитник"
    },
]

export const teamTypes = [
    {
        id: "less19",
        name: "Девушки до 19 лет",
    }, {
        id: "less17",
        name: "Девушки до 17 лет",
    }, {
        id: "less15",
        name: "Девушки до 15 лет",
    }, {
        id: "teamOfMasters",
        name: "Команда мастеров",
    }, {
        id: "nasTeam",
        name: "Наицональная сборная",
    }, {
        id: "u19",
        name: "Сборная РФ u-19",
    }, {
        id: "u18",
        name: "Сборная РФ u-18",
    }, {
        id: "u17",
        name: "Сборная РФ u-17",
    },
]