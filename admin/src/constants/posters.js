export const types = [
    {
        id: "hallOfFame",
        name: "Зал славы"
    }, {
        id: "supportGroup",
        name: "Группа поддержки"
    },  {
        id: "school",
        name: "Школа"
    }, {
        id: "shop",
        name: "Магазин"
    }, {
        id: "hallOfHistory",
        name: "Зал истории"
    }, {
        id: "hallOfModernity",
        name: "Зал современности"
    }, {
        id: "less13",
        name: "Девушки до 13 лет",
    }, {
        id: "main",
        name: "Главная страница",
    }, {
        id: "hi",
        name: "Приветственное слово",
    }
]
