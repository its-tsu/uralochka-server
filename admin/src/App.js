import React from 'react';
import './App.css';
import {
    Admin,
    Resource,
} from 'react-admin';
import LockIcon from '@material-ui/icons/Lock';
import dataProvider from './api';

import Dashboard from "./components/Dashboard"
import News from './components/Models/News';
import Achievements from './components/Models/Achievements';
import Dates from './components/Models/Dates';
import Partners from './components/Models/Partners';
import MainSlides from './components/Models/MainSlides';
import Championships from './components/Models/ChampionShips';
import MassMedia from './components/Models/MassMedia';
import Matches from './components/Models/Matches';
import Teams from './components/Models/Teams';
import Media from './components/Models/Media';
import Users from './components/Models/Users';
import BrandBooks from './components/Models/BrandBooks';
import Documents from './components/Models/Documents';
import MediaFolders from './components/Models/MediaFolders';
import NewsForFans from './components/Models/NewsForFans';
import Paragraphs from './components/Models/Paragraphs';
import People from './components/Models/People';
import Posters from './components/Models/Posters';
import Tours from './components/Models/Tours';
import authProvider from "./api/Auth";


const App = () => {
    return (
        <Admin dashboard={Dashboard} dataProvider={dataProvider} authProvider={authProvider}>
            {permissions => [
                <Resource options={{label: "Награды"}} name="achievements" {...Achievements}/>,
                <Resource options={{label: "Брэндбук"}} name="brand-books" {...BrandBooks}/>,
                <Resource options={{label: "Чемпионаты"}} name="champion-ships" {...Championships}/>,
                <Resource options={{label: "Важные даты"}} name="dates" {...Dates}/>,
                <Resource options={{label: "Документы"}} name="documents" {...Documents}/>,
                <Resource options={{label: "Глав. слайдер"}} name="main-slides" {...MainSlides}/>,
                <Resource options={{label: "СМИ о нас"}} name="mass-media" {...MassMedia}/>,
                <Resource options={{label: "Матчи"}} name="matches" {...Matches}/>,
                <Resource options={{label: "Медиа"}} name="media" {...Media}/>,
                <Resource options={{label: "Папки с медиафайлами"}} name="media-folders" {...MediaFolders}/>,
                <Resource options={{label: "Новости"}} name="news" {...News}/>,
                <Resource options={{label: "Новости для развлечения"}} name="news-for-fans" {...NewsForFans}/>,
                <Resource options={{label: "Абзацы"}} name="paragraphs" {...Paragraphs}/>,
                <Resource options={{label: "Партнеры"}} name="partners" {...Partners}/>,
                <Resource options={{label: "Люди"}} name="people" {...People}/>,
                <Resource options={{label: "Слайды сайта"}} name="posters" {...Posters}/>,
                <Resource options={{label: "Команды"}} name="teams" {...Teams}/>,
                <Resource options={{label: "Туры"}} name="tours" {...Tours}/>,
                permissions === "admin" && <Resource icon={LockIcon} options={{label: "Пользователи"}} name="users" {...Users}/>
                ]
            }
        </Admin>
    );
};

export default App;
