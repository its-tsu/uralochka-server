const Sequelize = require('sequelize');
let sequelize;

const Op = Sequelize.Op;
const operatorsAliases = {
    $between: Op.between,
    $or: Op.or,
};
if (process.env.DEV) {
    sequelize = new Sequelize(process.env.DATABASE_URL, {
        dialect: 'postgres',
        protocol: 'postgres',
        rejectUnauthorized: true,
        dialectOptions: {
            // ssl: true
        },
        operatorsAliases
    });
} else {
    sequelize = new Sequelize(process.env.DATABASE_URL);
}


module.exports = sequelize;