module.exports = {
    apps: [{
        name: "uralochka-server",
        script: "./index.js",
        watch: [
            "./config",
            "./constants",
            "./libs",
            "./models",
            "./routes",
            "./node_modules",
            "./index.js",
        ],
        env: {
            PORT: "8080",
            NO_IMG_URL: "/public/files/default.jpg",
        },
        env_ssl: {
            PORT: "443",
            NO_IMG_URL: "/public/files/default.jpg",
        },
        env_nossl: {
            PORT: "80",
            NO_IMG_URL: "/public/files/default.jpg",
        },
    }]
};