const {validationResult} = require('express-validator');

module.exports.validateAdmin = validations => {
    return async (req, res, next) => {
        await Promise.all(validations.map(validation => validation.run(req)));

        const errors = validationResult(req);
        if (errors.isEmpty()) {
            return next();
        }

        let errorMessage = "";
        errors.array().map(item => {
            errorMessage += item.msg;
        });

        res.status(422).send(errorMessage);
    };
};