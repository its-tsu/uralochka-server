const Sequelize = require('sequelize');

module.exports = {
    TEXT: {
        // Заголовок
        title: ({require = true} = {}) => ({
            type: Sequelize.TEXT,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Заголовок является обязательным!",
                },
                notEmpty: {
                    msg: "Заголовок является обязательным!",
                },
                len: {
                    args: [1, 500],
                    msg: "Заголовок должен быть от 1 до 500 символов!"
                },
            } : {},
        }),
        // Название
        name: ({require = true} = {}) => ({
            type: Sequelize.TEXT,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Название является обязательным!",
                },
                notEmpty: {
                    msg: "Название является обязательным!",
                },
                len: {
                    args: [1, 500],
                    msg: "Название должно быть от 1 до 500 символов!"
                },
            } : {},
        }),
        // Имя
        firstName: ({require = true} = {}) => ({
            type: Sequelize.TEXT,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Имя является обязательным!",
                },
                notEmpty: {
                    msg: "Имя является обязательным!",
                },
                len: {
                    args: [1, 500],
                    msg: "Имя должно быть от 1 до 500 символов!"
                },
            } : {},
        }),
        // Отчество
        middleName: ({require = true} = {}) => ({
            type: Sequelize.TEXT,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Отчество является обязательным!",
                },
                notEmpty: {
                    msg: "Отчество является обязательным!",
                },
                len: {
                    args: [1, 500],
                    msg: "Отчество должно быть от 1 до 500 символов!"
                },
            } : {},
        }),
        // Фамилия
        surname: ({require = true} = {}) => ({
            type: Sequelize.TEXT,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Фамилия является обязательной!",
                },
                notEmpty: {
                    msg: "Фамилия является обязательной!",
                },
                len: {
                    args: [1, 500],
                    msg: "Фамилия должна быть от 1 до 500 символов!"
                },
            } : {},
        }),
        // Описание
        description: ({require = true} = {}) => ({
            type: Sequelize.TEXT,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Описание является обязательным!",
                },
                notEmpty: {
                    msg: "Описание является обязательным!",
                },
                len: {
                    args: [1, 50000],
                    msg: "Описание должно быть от 1 до 50000 символов!"
                },
            } : {},
        }),
        // Текст
        text: ({require = true} = {}) => ({
            type: Sequelize.TEXT,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Текст является обязательным!",
                },
                notEmpty: {
                    msg: "Текст является обязательным!",
                },
                len: {
                    args: [1, 50000],
                    msg: "Текст должен быть от 1 до 50000 символов!"
                },
            } : {},
        }),
        link: ({require = true} = {}) => ({
            type: Sequelize.TEXT,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Ссылка на ресурс является обязательной!",
                },
                notEmpty: {
                    msg: "Ссылка на ресурс является обязательной!",
                },
                isUrl: {
                    msg: "Не верный формат ссылки!"
                }
            } : {
                isUrl: {
                    msg: "Не верный формат ссылки!"
                }},
        }),
        // Тип
        /**
         * @typedef Type
         * @property fieldName {String} Название типа в БД
         * @property ruName {String} Название типа для ошибки на русском
         */
        /**
         *
         * @param require
         * @param isIn {Type[]}
         */
        type: ({require = true, isIn = []} = {}) => ({
            type: Sequelize.TEXT,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Тип является обязательным!",
                },
                notEmpty: {
                    msg: "Тип является обязательным!",
                },
                isIn: {
                    // Соответствие в тексте ошибки и в название по позициям,
                    // то есть partner - это Партнер
                    args: [isIn.map(item => item.fieldName)],
                    msg: `Тип может являться только ${isIn.map(item => item.ruName).join(", ")}`
                }
            }: {},
        }),
    },

    NUMBER: {
        position: ({require = true} = {}) => ({
            type: Sequelize.INTEGER,
            allowNull: !require,
            validate: !require? {
                notNull: {
                    msg: "Место является обязательным!",
                },
                notEmpty: {
                    msg: "Место является обязательным!",
                },
                isInt: {
                    msg: "Место должно быть целым числом!"
                }
            }: {},
        }),
    },

    // Даты :
    DATE: {
        // Дата
        date: ({require = true, unique = false} = {}) => ({
            type: Sequelize.DATE,
            allowNull: !require,
            unique,
            validate: require ? {
                notNull: {
                    msg: "Дата является обязательной!",
                },
                notEmpty: {
                    msg: "Дата является обязательной!",
                },
                isDate: {
                    msg: "Дата пришла не в том формате!",
                },
            } : {},
        }),
        // Дата с указанием времени
        dateWithTime: ({require = true} = {}) => ({
            type: Sequelize.DATE,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Дата является обязательной!",
                },
                notEmpty: {
                    msg: "Дата является обязательной!",
                },
                isDate: {
                    msg: "Дата пришла не в том формате!",
                },
            } : {},
        }),
        // День рождения
        birthday: ({require = true} = {}) => ({
            type: Sequelize.DATE,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Дата является обязательной!",
                },
                notEmpty: {
                    msg: "Дата является обязательной!",
                },
                isDate: {
                    msg: "Дата пришла не в том формате!",
                },
            } : {},
        }),
    },

    BOOLEAN: {
        isViewOnMainPage: ({require = true} = {}) => ({
            type: Sequelize.BOOLEAN,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Состояние является обязательным!",
                },
                notEmpty: {
                    msg: "Состояние является обязательным!",
                },
            }: {},
        }),
        isRFPlayer: ({require = true} = {}) => ({
            type: Sequelize.BOOLEAN,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Состояние является обязательным!",
                },
                notEmpty: {
                    msg: "Состояние является обязательным!",
                },
            }: {},
        }),
        isTeamOfMasters: ({require = true} = {}) => ({
            type: Sequelize.BOOLEAN,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Состояние является обязательным!",
                },
                notEmpty: {
                    msg: "Состояние является обязательным!",
                },
            }: {},
        }),
        isViewOnTeamPage: ({require = true} = {}) => ({
            type: Sequelize.BOOLEAN,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Обязательно необходимо указать, нужно ли отображать тренера на странице команды!",
                },
                notEmpty: {
                    msg: "Обязательно необходимо указать, нужно ли отображать тренера на странице команды!",
                },
            }: {},
        }),
    },

    // Файлы :
    FILE: {
        // Основное изображение
        imgUrl: ({require = true} = {}) => ({
            type: Sequelize.STRING,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Изображение является обязательным!",
                },
                notEmpty: {
                    msg: "Изображение является обязательным!",
                },
            }: {},
        }),
        // Вторичное изображение
        secondImgUrl: ({require = true} = {}) => ({
            type: Sequelize.STRING,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Вторичное изображение является обязательным!",
                },
                notEmpty: {
                    msg: "Вторичное изображение является обязательным!",
                },
            }: {},
        }),
        // Аватарка
        avatarUrl: ({require = true} = {}) => ({
            type: Sequelize.STRING,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Аватарка является обязательным!",
                },
                notEmpty: {
                    msg: "Аватарка изображение является обязательным!",
                },
            }: {},
        }),
        // Фотография таблицы результатов
        tableImgUrl: ({require = true} = {}) => ({
            type: Sequelize.STRING,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Фото таблицы является обязательной!",
                },
                notEmpty: {
                    msg: "Фото таблицы является обязательной!",
                },
            }: {},
        }),
        protocolImgUrl: ({require = true} = {}) => ({
            type: Sequelize.STRING,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Фото протокола является обязательной!",
                },
                notEmpty: {
                    msg: "Фото протокола является обязательной!",
                },
            }: {},
        }),
        // Фотография таблицы результатов
        fileUrl: ({require = true} = {}) => ({
            type: Sequelize.STRING,
            allowNull: !require,
            validate: require ? {
                notNull: {
                    msg: "Файл является обязательным!",
                },
                notEmpty: {
                    msg: "Файл является обязательным!",
                },
            }: {},
        }),
    },
};
