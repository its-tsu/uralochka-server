function sequelizeValidationError(errorsArray) {
    let errors = "";
    errorsArray.map(item => {
        errors += `${item.message}\n`;
    });
    return errors;
}

function sequelizeUniqueConstraintError(errorsObj) {
    let errors = "";
    switch (errorsObj.original.table) {
        case "dates":
            errors += "Дата должна быть уникальной!\n";
            break;
    }
    return errors;
}

function errorHandler(error, res) {
    let normilizeError = "";
    switch (error.name) {
        case "SequelizeValidationError":
            normilizeError = sequelizeValidationError(error.errors);
            return res.status(422).send(normilizeError);
        case "SequelizeUniqueConstraintError":
            normilizeError = sequelizeUniqueConstraintError(error);
            return res.status(422).send(normilizeError);
        default:
            return res.status(500).send("Все плохо, можете обратиться к разработчику");
    }
}

errorHandler.sequelizeValidationError = sequelizeValidationError;

module.exports.errorHandler = errorHandler;
