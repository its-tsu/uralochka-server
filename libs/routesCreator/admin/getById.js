const express = require("express");

module.exports.getById = function (path, model, options={}) {
    const router = express.Router();
    router.get(`${path}:id`, async function (req, res) {
        const entitie = await model.findOne({
            where: {id: req.params.id},
        });
        res.json(entitie);
    });
    return router;
}