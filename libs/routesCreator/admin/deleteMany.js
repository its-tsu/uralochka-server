const express = require("express");
const {deleteFile} = require("../../fileWork");
const {log} = require("../../log");

module.exports.deleteMany = function (path, model, options={}) {
    const router = express.Router();
    router.delete(`${path}`, async function (req, res) {
        const entities = await model.findAll({where: JSON.parse(req.query.filter)});

        entities.map(entity => {
            entity.fileUrl && deleteFile(entity.fileUrl);
        });

        await model.destroy({where: JSON.parse(req.query.filter)});
        res.json("ok");
        try {
            log.info(`Пользователь с id: ${req.user.id} удалил сущности (${model.name} с фильтром: ${req.query.filter})!`)
        } catch (e) {
            log.error({error: e.toString(), path, _message: "Ошибка логирования!"});
        }
    });
    return router;
}