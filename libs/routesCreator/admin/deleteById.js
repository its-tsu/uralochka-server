const express = require("express");
const {deleteFile} = require("../../fileWork");
const {log} = require("../../log");

module.exports.deleteById = function (path, model, options = {}) {
    const router = express.Router();
    router.delete(`${path}:id`, async function (req, res) {
        const entity = await model.findOne({
            where: {id: req.params.id},
        });

        if(!entity) {
            res.json("ok!");
            try {
                return  log.info(`Пользователь с id: ${req.user.id} НЕ удалил сущность, так как ее нет (${model.name})!`)
            } catch (e) {
                return  log.error({error: e.toString(), path, _message: "Ошибка логирования!"});
            }
        }

        // Удаление файла, если таковой присутствует
        entity.fileUrl && deleteFile(entity.fileUrl);

        await model.destroy({
            where: {id: req.params.id},
        });
        res.json("ok!");
        try {
            log.info(`Пользователь с id: ${req.user.id} удалил сущность (${model.name} с id: ${entity.id})!`)
        } catch (e) {
            log.error({error: e.toString(), path, _message: "Ошибка логирования!"});
        }
    });
    return router;
}
