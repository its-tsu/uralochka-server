const express = require("express");
const {saveFile} = require("../../fileWork");
const {errorHandler} = require("../../errorHandler");
const {log} = require("../../log");

module.exports.create = function (path, model, options={}) {
    const router = express.Router();
    router.post(path, async function (req, res) {
        try {
            let fileFields = ['file', 'secondImg', 'img', 'table', 'protocolImg'];
            let files = {};
            fileFields.forEach(field => {
                if(req.body[field]) {
                    files[field+"Url"] = saveFile("public/files", req.body[field]).fileUrl;
                }
            });
            let entity;
            if(options.fields) {
                entity = await model.create({...req.body, ...files}, {fields: options.fields});
            } else {
                entity = await model.create({...req.body, ...files});
            }
            res.json(entity);
            try {
                log.info(`Пользователь с id: ${req.user.id} создал новую сущность (${model.name})!`)
            } catch (e) {
                log.error({error: e.toString(), path, _message: "Ошибка логирования!"});
            }
        } catch (e) {
            errorHandler(e, res);
            try {
                log.info(`Пользователь с id: ${req.user.id} НЕ создал новую сущность (${model.name})!`);
            } catch (e) {
                log.error({error: e.toString(), path, _message: "Ошибка логирования!"});
            }
            console.log(e);
        }
    });
    return router;
};
