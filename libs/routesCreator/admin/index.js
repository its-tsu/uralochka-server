const {getById} =require("./getById");
const {filter} =require("./filter");
const {create} =require("./create");
const {deleteById} =require("./deleteById");
const {deleteMany} =require("./deleteMany");
module.exports = {
    getById,
    filter,
    create,
    deleteById,
    deleteMany,
};