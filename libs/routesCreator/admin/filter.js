const express = require("express");
const sequelize = require("sequelize");

module.exports.filter = function (path, model, options={}) {
    const router = express.Router();
    router.get(`${path}`, async function (req, res) {
        let {sort, range, filter} = req.query;
        if(range) {
            range = JSON.parse(range);
        } else {
            range = [0, 20]
        }
        const limit = range[1] - range[0] + 1;
        const offset = range[0];
        if(sort) {
            sort = JSON.parse(sort);
        } else {
            sort = ["id", "DESC"]
        }
        if(filter) {
            filter = JSON.parse(filter);
            if(options.searchableFieldsSubStr && options.searchableFieldsSubStr.length) {
                options.searchableFieldsSubStr.map(nameField => {
                    if(filter[nameField])
                        filter[nameField] = sequelize.where(sequelize.fn('LOWER', sequelize.col(nameField)), 'LIKE', '%' + filter[nameField].toLowerCase() + '%');
                })
            }
        } else {
            filter = {};
        }
        let include = [];
        if(options.include) {
            include = options.include;
        }
        const entities = await model.findAll({
            where: {...filter},
            order: [sort],
            limit,
            offset,
            include,
        });

        const entitiesCount = await model.count({
            where: {...filter},
            order: [sort],
            limit,
            offset,
            include,
        });

        res.setHeader("Access-Control-Expose-Headers", `Content-Range`);
        res.setHeader("Content-Range", `${range[0]}-${range[1]}/${entitiesCount}`);
        res.json(entities);
    });
    return router;
}