const userRoutes = require("./user");
const adminRoutes = require("./admin");
const express = require("express");


const routesCreator = {
    user: userRoutes,
    admin: adminRoutes,
};

/**
 *
 * @typedef {Object} Inc
 * @property {Object} model - Объект модели.
 * @property {String} as - Имя поля.
 */

/**
 *
 *
 * @typedef {Object} Route
 * @property {String} type - Типа роута (имя).
 * @property {Function} DTO - Функция преобразования ответа.
 * @property {Inc[]} include - Список моделей для импорта
 * @property {String} postfix - Потфикс роута, чтобы не дублировались
 * @property {String[]} searchableFieldsSubStr - Список полей для поиска по подстроке.
 *
 * @param {String} path
 * @param {String} model
 * @param {String} type
 * @param {Route[]|String[]} routes
 * @returns {Router}
 */
module.exports = function (path, model, type, routes = []) {
    const router = express.Router();
    for(let route of routes) {
        if(typeof route === "string") {
            router.use(routesCreator[type][route](path, model));
        } else {
            router.use(routesCreator[type][route.type](path, model, route));
        }
    }
    return router;
};