const express = require("express");
const sequelize = require("sequelize");
const { query } = require("express-validator");
const {validate} = require("../../validate");

module.exports.filter = function (path, model, options={}) {
    const router = express.Router();
    router.get(`${path}`, validate([
        query("sort")
            .optional()
            .isJSON().withMessage("Поле не приводится к типу JSON")
            .custom(value => Array.isArray(JSON.parse(value)) && JSON.parse(value).length === 2).withMessage("Поле должно быть JSON масииов из 2 элементов!"),
        query("filter")
            .optional()
            .isJSON().withMessage("Поле не приводится к типу JSON")
            .custom(value => !Array.isArray(JSON.parse(value))).withMessage("Поле должно быть JSON объектом"),
        query("limit")
            .optional()
            .isInt({min: 1, max: 100}).withMessage("Поле должно быть целым числом от 1 до 100!")
            .toInt(),
        query("offset")
            .optional()
            .isInt({min: 0}).withMessage("Поле должно быть целым положительным числом!")
            .toInt(),
    ]), async function (req, res) {
        console.log(req.query);
        let {sort, limit = 20, offset = 0, filter} = req.query;
        if(sort) {
            sort = JSON.parse(sort);
        } else {
            sort = ["id", "DESC"]
        }
        if(filter) {
            filter = JSON.parse(filter);
            if(options.searchableFieldsSubStr && options.searchableFieldsSubStr.length) {
                options.searchableFieldsSubStr.map(nameField => {
                    if(filter[nameField])
                        filter[nameField] = sequelize.where(sequelize.fn('LOWER', sequelize.col(nameField)), 'LIKE', '%' + filter[nameField].toLowerCase() + '%');
                })
            }
        } else {
            filter = {};
        }
        let include = [];
        if(options.include) {
            include = options.include;
        }
        let entities = await model.findAll({
            where: {...filter},
            order: [sort],
            limit,
            offset,
            include,
            attributes: { exclude: ["deletedAt", "updatedAt", "createdAt"] }
        });

        if(typeof options.DTO === "function") {
            entities = entities.map(item => options.DTO(item.get({plain: true})));
        }

        const entitiesCount = await model.count({
            where: {...filter},
            order: [sort],
            limit,
            offset,
            include,
            attributes: { exclude: ["deletedAt", "updatedAt", "createdAt"] }
        });

        res.setHeader("Access-Control-Expose-Headers", `Content-Range`);
        res.setHeader("Content-Range", `${offset}-${offset+limit}/${entitiesCount}`);
        res.json({success: true, data: entities});
    });
    return router;
}