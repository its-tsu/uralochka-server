const {filter} = require("./filter");
const {getById} = require("./getById");

module.exports = {
    filter,
    getById,
};