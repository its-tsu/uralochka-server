const express = require("express");

module.exports.getById = function (path, model, options={}) {
    const router = express.Router();
    let include = [];
    if(options.include) {
        include = options.include;
    }
    router.get(`${path}:id`, async function (req, res) {
        let entity = await model.findOne({
            where: {id: req.params.id},
            attributes: { exclude: ["deletedAt", "updatedAt", "createdAt"] },
            include
        });
        if(typeof options.DTO === "function") {
            entity = options.DTO(entity.get({plain: true}));
        }
        res.json({success: true, data: entity});
    });
    return router;
}