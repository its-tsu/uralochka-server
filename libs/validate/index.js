const {validationResult} = require('express-validator');

module.exports.validate = validations => {
    return async (req, res, next) => {
        await Promise.all(validations.map(validation => validation.run(req)));

        const errors = validationResult(req);
        if (errors.isEmpty()) {
            return next();
        }

        let errorsObject = {};
        errors.array().map(item => {
            errorsObject[item.param] = item.msg;
        })

        res.status(422).json({ success: false, errors: errorsObject });
    };
};