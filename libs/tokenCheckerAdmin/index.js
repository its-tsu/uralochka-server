const jwt = require('jsonwebtoken');
const {Models} = require("../../models");

module.exports = (...roles) => async (req,res,next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, process.env.SECRET, async function(err, decoded) {
            if (err) {
                return res.status(401).json("Вы не авторизированны!1");
            }
            req.decoded = decoded;
            if(typeof decoded !== "object" || !decoded.userId) {
                return res.status(401).json("Вы не авторизированны!2");
            }
            const user = await Models.users.findOne({where: {id: decoded.userId, token}});
            if(!user){
                return res.status(401).json("Вы не авторизированны!3");
            }
            const set = new Set(roles);
            if(!set.has(user.role)) {
                return res.status(401).send("Вам не хватает прав!");
            }
            req.user = user;
            next();
        });
    } else {
        return res.status(401).json({success: false, errors: {}, _message: "Вы не авторизированны!4"});
    }
}