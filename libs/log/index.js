const winston = require('winston');
const path = require('path');

module.exports.log = winston.createLogger({
    format: winston.format.printf(info => typeof info.message === "string" ?
        JSON.stringify({date: new Date(), message: info.message, level: info.level})
    : JSON.stringify({date: new Date(),  data: info.message, level: info.level})),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: path.join(__dirname, '../../logs/app.log'), maxsize: 5242880 }),
        new winston.transports.File({ filename: path.join(__dirname, '../../logs/error.log'), level: "error", maxsize: 5242880 }),
    ]
});