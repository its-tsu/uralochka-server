const fs = require("fs");
const path = require("path");
const { v4: uuidv4 } = require('uuid');
const {log} = require('../log');

module.exports.saveFile = function (filePath = "images", file) {
    try {
        let fileType = file.title.split(".").pop();
        const fileName = `${uuidv4()}.${fileType}`;
        fs.writeFileSync(path.join(global.staticPath, filePath, fileName), new Buffer(file.src.split(",").pop(), 'base64'), );
        return {fileUrl: path.join("/", filePath, fileName).replace(/\\/g, "\/"), fileName};
    } catch (err) {
        console.error(err);
        log.error({error: err.toString(), path: __dirname, _message: "Ошибка сохранения фото!"});
        throw new Error(err);
    }
};

module.exports.deleteFile = function (filePath) {
    fs.unlinkSync(path.join(global.staticPath, filePath));
};
