const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");

const Model = Sequelize.Model;

class MassMedia extends Model {}

MassMedia.init({
    title: fieldsCreator.TEXT.title(),
    text: fieldsCreator.TEXT.text(),
    date: fieldsCreator.DATE.date(),
    link: fieldsCreator.TEXT.link(),
    imgUrl: fieldsCreator.FILE.imgUrl(),
}, {
    sequelize,
    paranoid: true,
    modelName: 'massMedia',
});

module.exports = {
    MassMedia,
};