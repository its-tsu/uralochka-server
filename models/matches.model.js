const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");

const Model = Sequelize.Model;

class Matches extends Model {}

Matches.init({
    name: fieldsCreator.TEXT.name(),
    description: fieldsCreator.TEXT.description(),
    protocolImgUrl: fieldsCreator.FILE.protocolImgUrl(),
    date: fieldsCreator.DATE.dateWithTime(),
    // Ссылка на прямую трансляцию
    link: fieldsCreator.TEXT.link({require: false}),
    leftPoints: {
        type: Sequelize.INTEGER,
        allowNull:false,
        validate: {
            isInt: {
                msg: "Счет левой комадны должен являться целым числом!",
            },
            notEmpty: {
                msg: "Счет левой комадны необходимо указать обязательно!"
            },
            notNull: {
                msg: "Счет левой комадны необходимо указать обязательно!"
            }
        },
    },
    rightPoints: {
        type: Sequelize.INTEGER,
        allowNull:false,
        validate: {
            isInt: {
                msg: "Счет правой комадны должен являться целым числом!",
            },
            notEmpty: {
                msg: "Счет правой комадны необходимо указать обязательно!"
            },
            notNull: {
                msg: "Счет правой комадны необходимо указать обязательно!"
            }
        },
    },
    leftTeamId: {
        type: Sequelize.INTEGER,
        allowNull:false,
        references: {
            model: 'teams',
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
        },
        validate: {
            notNull: {
                msg: "Левая команда является обязательной!",
            },
            notEmpty: {
                msg: "Левая команда является обязательной!",
            },
        },
    },
    rightTeamId: {
        type: Sequelize.INTEGER,
        allowNull:false,
        references: {
            model: 'teams',
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
        },
        validate: {
            notNull: {
                msg: "Правая команда является обязательной!",
            },
            notEmpty: {
                msg: "Правая команда является обязательной!",
            },
        },
    },
    tourId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: 'tours',
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
        },
        validate: {
            notNull: {
                msg: "Тур является обязательным!",
            },
            notEmpty: {
                msg: "Тур является обязательным!",
            },
        },
    },
    mediaFolderId: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
            model: 'media-folders',
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
        },
    },
}, {
    sequelize,
    paranoid: true,
    modelName: 'matches',
});

module.exports = {
    Matches,
};
