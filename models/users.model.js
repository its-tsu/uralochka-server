const Sequelize = require('sequelize');
const sequelize = require('../config/db');

const Model = Sequelize.Model;

class Users extends Model {}

// TODO Допилить пользователя и его валидацию.
Users.init({
    login: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
            notNull: {
                msg: "login является обязательным!",
            },
            notEmpty: {
                msg: "login является обязательным!",
            },
            len: {
                args: [1, 500],
                msg: "login должен быть от 1 до 500 символов!"
            },
        },
    },
    password: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
            notNull: {
                msg: "password является обязательным!",
            },
            notEmpty: {
                msg: "password является обязательным!",
            },
            len: {
                args: [1, 500],
                msg: "password должен быть от 1 до 500 символов!"
            },
        },
    },
    token: {
        type: Sequelize.TEXT,
        defaultValue: ""
    },
    role: {
        type: Sequelize.TEXT,
        defaultValue: "moderator"
    },
}, {
    sequelize,
    paranoid: true,
    modelName: 'users',
});

module.exports = {
    Users,
};