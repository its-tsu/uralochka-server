const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");
const {types} = require("../constants/models/posters");

const Model = Sequelize.Model;

class Posters extends Model {
}

Posters.init({
    imgUrl: fieldsCreator.FILE.imgUrl(),
    name: fieldsCreator.TEXT.name(),
    type: fieldsCreator.TEXT.type({isIn: types}),
}, {
    sequelize,
    paranoid: true,
    modelName: 'posters',
});

module.exports = {
    Posters,
};