const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");
const {types} = require("../constants/models/media");

const Model = Sequelize.Model;

class Media extends Model {}

Media.init({
    name: fieldsCreator.TEXT.name(),
    link: fieldsCreator.TEXT.link({require: false}),
    type: fieldsCreator.TEXT.type({isIn: types}),
    date: fieldsCreator.DATE.date(),
    imgUrl: fieldsCreator.FILE.imgUrl(),
    mediaFolderId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: 'media-folders',
            key: 'id',
        },
        validate: {
            notNull: {
                msg: "Папка является обязательной!",
            },
            notEmpty: {
                msg: "Папка является обязательной!",
            },
        },
    },
}, {
    sequelize,
    paranoid: true,
    modelName: 'media',
});

module.exports = {
    Media,
};
