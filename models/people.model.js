const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");
const {types, teamTypes} = require("../constants/models/people");

const Model = Sequelize.Model;

class People extends Model {
}

People.init({
    imgUrl: fieldsCreator.FILE.imgUrl(),
    secondImgUrl: fieldsCreator.FILE.secondImgUrl(),
    firstName: fieldsCreator.TEXT.firstName(),
    middleName: fieldsCreator.TEXT.middleName({require: false}),
    surname: fieldsCreator.TEXT.surname({require: false}),
    firstParagraph: fieldsCreator.TEXT.text({require: false}),
    secondParagraph: fieldsCreator.TEXT.text({require: false}),

    // Должность (просто текст, он не обязателе, нужен только админу)
    position: {
        type: Sequelize.TEXT,
    },

    type: fieldsCreator.TEXT.type({isIn: types}),

    // Для игроков и для тренеров обязательно
    teamType: fieldsCreator.TEXT.type({isIn: teamTypes}),
    achievements: fieldsCreator.TEXT.text(),

    // Отображать ли тренера на странице команды
    isViewOnTeamPage: fieldsCreator.BOOLEAN.isViewOnTeamPage(),

}, {
    sequelize,
    paranoid: true,
    modelName: 'people',
});

module.exports = {
    People,
};