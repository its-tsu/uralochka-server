(async function () {
    const path = require("path");
    const fs = require("fs");
    const { randomInteger } = require("../../libs/randomInteger");

    const deleteFolderRecursive = function(pathFolder) {
        if (fs.existsSync(pathFolder)) {
            fs.readdirSync(pathFolder).forEach((file, index) => {
                const curPath = path.join(pathFolder, file);
                if (fs.lstatSync(curPath).isDirectory()) { // recurse
                    deleteFolderRecursive(curPath);
                } else { // delete file
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(pathFolder);
        }
    };

    function getRandomFile() {
        let files = fs.readdirSync(path.join(__dirname, "files"));
        let i = randomInteger(0, files.length - 1);
        return {title: files[i], src: fs.readFileSync(path.join(__dirname, "files", files[i]), { encoding: 'base64' })};
    }

    try {
        global.staticPath = path.join(__dirname, "../../static");
        require("dotenv").config({path: path.join(__dirname, "../../config/.env")});
        const {sync, Models} = require("../index.js");
        const {saveFile} = require("../../libs/fileWork");
        const moment = require("moment");
        if(fs.existsSync(path.join(__dirname, "../../static/public", "files"))) {
            deleteFolderRecursive(path.join(__dirname, "../../static/public", "files"));
        }
        fs.mkdirSync(path.join(__dirname, "../../static/public", "files"));

        await sync(true);


        const {
            achievements,
            brandBooks,
            championships,
            dates,
            documents,
            mainSlides,
            massMedia,
            matches,
            media,
            mediaFolders,
            news,
            newsForFans,
            paragraphs,
            partners,
            people,
            posters,
            teams,
            tours,
            users,
            } = Models;


        for (let i = 1; i < 6; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await achievements.create({
                secondImgUrl: fileUrl,
                name: `Достижение № ${i}`,
                position: i,
                firstParagraph: "ТекстТекст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст  ",
                championships: "Достижение №1~~Достижение №2",
            });
        }

        for (let i = 1; i < 6; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await brandBooks.create({
                name: `Брендбук № ${i}`,
                imgUrl: fileUrl,
            });
        }

        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await dates.create({
                description: "Описание очень важной даты. Описание очень важной даты.",
                date: moment().add(i * 3 - 1, "day").toDate(),
                imgUrl: fileUrl,
            });
        }

        const {types: documentsTypes} = require("../../constants/models/documents");
        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await documents.create({
                name: "Название для файла.png",
                type: documentsTypes[randomInteger(0, documentsTypes.length - 1)].fieldName,
                fileUrl: fileUrl,
            });
        }

        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await mainSlides.create({
                name: `Слайд # ${i}`,
                imgUrl: fileUrl,
            });
        }

        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await massMedia.create({
                title: `Заголовок # ${i}`,
                text: `ООООООчень интересный текст. ООООООчень интересный текст. ООООООчень интересный текст. ООООООчень интересный текст.`,
                date: moment().add(i * 3 - 1, "day").toDate(),
                link: `https://google.com`,
                imgUrl: fileUrl,
            });
        }



        const {types: newsTypes} = require("../../constants/models/news");
        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await news.create({
                title: `ОООчень важный заголовок`,
                type: newsTypes[randomInteger(0, newsTypes.length - 1)].fieldName,
                text: "Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. ",
                date: moment().subtract(i * 20 - 1, "day").toDate(),
                imgUrl: fileUrl,
            });
        }

        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await newsForFans.create({
                title: `ОООчень важный заголовок`,
                text: "Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. ",
                date: moment().subtract(i * 20 - 1, "day").toDate(),
                imgUrl: fileUrl,
            });
        }

        const {types: paragraphsTypes, positions: paragraphsPositions} = require("../../constants/models/paragraphs");
        for (let i = 1; i < 5; i++) {
            await paragraphs.create({
                text: "Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. ",
                type: paragraphsTypes[randomInteger(0, paragraphsTypes.length - 1)].fieldName,
                position: paragraphsPositions[randomInteger(0, paragraphsPositions.length - 1)].fieldName,
            });
        }

        const {types: partnersTypes} = require("../../constants/models/partners");
        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await partners.create({
                imgUrl: fileUrl,
                name: `Название ${i}`,
                link: "http://google.com",
                type: partnersTypes[randomInteger(0, partnersTypes.length - 1)].fieldName,
            });
        }


        const {types: peopleTypes, teamTypes} = require("../../constants/models/people");
        for (let i = 1; i < 100; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            const {fileUrl: fileUrl2} = saveFile("public/files", getRandomFile());
            await people.create({
                imgUrl: fileUrl,
                secondImgUrl: fileUrl2,
                firstName: "Имя",
                middleName: randomInteger(0,2) === 0 ? null : "Отчество",
                surname: randomInteger(0,2) === 0 ? null : "Фамилия",
                firstParagraph: "Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. ",
                secondParagraph: randomInteger(0,1) === 1 ? "Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. Очень интересный текст. " : null,
                birthday: moment().subtract(randomInteger(12, 18), "year").toDate(),
                position: "Должность",
                type: peopleTypes[randomInteger(0, peopleTypes.length - 1)].fieldName,
                teamType: teamTypes[randomInteger(0, teamTypes.length - 1)].fieldName,
                achievements: "Достижение №1~~Достижение №2",
                isViewOnTeamPage: !!randomInteger(0, 1),
            });
        }

        const {types: postersTypes} = require("../../constants/models/posters");
        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await posters.create({
                imgUrl: fileUrl,
                name: "Название",
                type: postersTypes[randomInteger(0, postersTypes.length - 1)].fieldName,
            });
        }




        await users.create({
            login: "admin",
            password: "admin",
            role: "admin",
        });


        const {types: championshipsTypes} = require("../../constants/models/championships");
        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            const {fileUrl: fileUrl2} = saveFile("public/files", getRandomFile());
            await championships.create({
                tableUrl: fileUrl,
                imgUrl: fileUrl2,
                date: moment().add(i - 1, "day").toDate(),
                title: `Чемпионар россии № ${i}`,
                description: "Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание Описание ",
                type: championshipsTypes[randomInteger(0, championshipsTypes.length - 1)].fieldName,
                isActiveOnMainPage: randomInteger(0, 2) === 0,
            });
        }


        for (let i = 1; i < 11; i++) {
            await tours.create({
                name: "Название",
                date: moment().subtract(i * 20 - 1, "day").toDate(),
                title: "Заголовок для тура",
                championshipId: randomInteger(1,10),
                location: "г. Тольятти",
            });
        }

        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await teams.create({
                imgUrl: fileUrl,
                name: "Название",
            });
        }

        for (let i = 1; i < 11; i++) {
            await mediaFolders.create({
                name: `Название медиа Папки # ${i}`,
                date: moment().add(i * 3 - 1, "day").toDate(),
            });
        }

        const {types: mediaTypes} = require("../../constants/models/media");
        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await media.create({
                name: `Название медиа # ${i}`,
                link: "https://google.com",
                type: mediaTypes[randomInteger(0, mediaTypes.length - 1)].fieldName,
                date: moment().add(i * 3 - 1, "day").toDate(),
                imgUrl: fileUrl,
                mediaFolderId: randomInteger(1, 3),
            });
        }

        for (let i = 1; i < 11; i++) {
            const {fileUrl} = saveFile("public/files", getRandomFile());
            await matches.create({
                name: "Название матча",
                description: `Описание матча. Описание матча. Описание матча. Описание матча. Описание матча. Описание матча. `,
                date: moment().add(i * 3 - 1, "day").toDate(),
                protocolImgUrl: fileUrl,
                leftPoints: randomInteger(0, 10),
                rightPoints: randomInteger(0, 10),
                leftTeamId: randomInteger(1, 5),
                rightTeamId: randomInteger(6, 10),
                tourId: randomInteger(1, 10),
                mediaFolderId: randomInteger(0, 1) ? randomInteger(1, 10): null,
                link: "https://google.com",
            });
        }

        process.exit(0);
    } catch (e) {
        console.log(e);
    }
})();
