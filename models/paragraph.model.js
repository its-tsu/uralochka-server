const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");
const {types, positions} = require("../constants/models/paragraphs");

const Model = Sequelize.Model;

class Paragraphs extends Model {
}

Paragraphs.init({
    text: fieldsCreator.TEXT.text(),
    type: fieldsCreator.TEXT.type({
        isIn: types,
    }),
    position: fieldsCreator.TEXT.type({
        isIn: positions,
    }),

}, {
    sequelize,
    paranoid: true,
    modelName: 'paragraphs',
});

module.exports = {
    Paragraphs,
};