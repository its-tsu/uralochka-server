const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");

const Model = Sequelize.Model;

class Tours extends Model {
}

Tours.init({
    // Название тура
    name: fieldsCreator.TEXT.name(),
    // date: fieldsCreator.DATE.date(),
    title: fieldsCreator.TEXT.title(),
    championshipId: {
        type: Sequelize.INTEGER,
        allowNull:false,
        references: {
            model: 'championships',
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
        },
        validate: {
            notNull: {
                msg: "Соревнование является обязательным!",
            },
            notEmpty: {
                msg: "Соревнование является обязательным!",
            },
        },
    },
    location: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
            notNull: {
                msg: "Место проведения является обязательным!",
            },
            notEmpty: {
                msg: "Место проведения является обязательным!",
            },
            len: {
                args: [1, 500],
                msg: "Место проведения должно быть от 1 до 500 символов!"
            },
        },
    },
}, {
    sequelize,
    paranoid: true,
    modelName: 'tours',
});

module.exports = {
    Tours,
};
