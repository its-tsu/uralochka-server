const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");
const {types} = require("../constants/models/championships");

const Model = Sequelize.Model;

class Championships extends Model {
}

Championships.init({
    tableUrl: fieldsCreator.FILE.tableImgUrl(),
    imgUrl: fieldsCreator.FILE.imgUrl(),
    date: fieldsCreator.DATE.date(),
    title: fieldsCreator.TEXT.title(),
    description: fieldsCreator.TEXT.description(),
    type: fieldsCreator.TEXT.type({isIn: types}),
    // Отображать на главной странице?
    isActiveOnMainPage: fieldsCreator.BOOLEAN.isViewOnMainPage(),
}, {
    sequelize,
    paranoid: true,
    modelName: 'championships',
});

module.exports = {
    Championships,
};