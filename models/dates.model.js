const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");

const Model = Sequelize.Model;

class Dates extends Model {}

Dates.init({
    description: fieldsCreator.TEXT.description(),
    date: fieldsCreator.DATE.date({unique: true}),
    imgUrl: fieldsCreator.FILE.imgUrl(),
}, {
    sequelize,
    paranoid: true,
    modelName: 'dates',
});

module.exports = {
    Dates,
};