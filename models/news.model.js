const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");
const {types} = require("../constants/models/news");

const Model = Sequelize.Model;

class News extends Model {}

News.init({
    title: fieldsCreator.TEXT.title(),
    type: fieldsCreator.TEXT.type({isIn: types}),
    text: fieldsCreator.TEXT.text(),
    date: fieldsCreator.DATE.date(),
    imgUrl: fieldsCreator.FILE.imgUrl(),
}, {
    sequelize,
    paranoid: true,
    modelName: 'news',
});

module.exports = {
    News,
};