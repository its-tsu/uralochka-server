const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");
const {types} = require("../constants/models/partners");

const Model = Sequelize.Model;

class Partners extends Model {
}

Partners.init({
    imgUrl: fieldsCreator.FILE.imgUrl(),
    name: fieldsCreator.TEXT.name(),
    link: fieldsCreator.TEXT.link(),
    type: fieldsCreator.TEXT.type({isIn: types}),
}, {
    sequelize,
    paranoid: true,
    modelName: 'partners',
});

module.exports = {
    Partners,
};