const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");

const Model = Sequelize.Model;

class MediaFolders extends Model {}

MediaFolders.init({
    name: fieldsCreator.TEXT.name(),
    date:fieldsCreator.DATE.date(),
}, {
    sequelize,
    paranoid: true,
    modelName: 'media-folders',
});

module.exports = {
    MediaFolders,
};