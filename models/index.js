const {Achievements} = require("./achievements.model");
const {BrandBooks} = require("./brandBooks.model");
const {Championships} = require("./championship.model");
const {Dates} = require("./dates.model");
const {Documents} = require("./documents.model");
const {MainSlides} = require("./mainSlides.model");
const {MassMedia} = require("./massMedia.model");
const {Matches} = require("./matches.model");
const {Media} = require("./media.model");
const {MediaFolders} = require("./mediaFolders.model");
const {News} = require("./news.model");
const {NewsForFans} = require("./newsForFans.model");
const {Paragraphs} = require("./paragraph.model");
const {Partners} = require("./partners.model");
const {People} = require("./people.model");
const {Posters} = require("./posters.model");
const {Teams} = require("./teams.model");
const {Tours} = require("./tours.model");
const {Users} = require("./users.model");


module.exports.sync = async (FORCE) => {
    let force = false;
    let alter = false;

    await Championships.hasMany(Tours);
    await Tours.belongsTo(Championships, {foreignKey: "championshipId", as: "championship"});

    await Tours.hasMany(Matches);
    await Matches.belongsTo(Tours, {foreignKey: "tourId", as: "tour"});

    await Teams.hasMany(Matches);
    await Matches.belongsTo(Teams, {foreignKey: "leftTeamId", as: "leftTeam"});
    await Matches.belongsTo(Teams, {foreignKey: "rightTeamId", as: "rightTeam"});

    await MediaFolders.hasMany(Media);
    await Media.belongsTo(MediaFolders, {foreignKey: "mediaFolderId", as: "mediaFolder"});


    // Одиночные таблицы в правильном порядке
    await Achievements.sync({force, alter});
    await BrandBooks.sync({force, alter});
    // Championships
    await Dates.sync({force, alter});
    await Documents.sync({force, alter});
    await MainSlides.sync({force, alter});
    await MassMedia.sync({force, alter});
    // Matches
    await MassMedia.sync({force, alter});
    // Media
    // MediaFolders
    await News.sync({force, alter});
    await NewsForFans.sync({force, alter});
    await Paragraphs.sync({force, alter});
    await Partners.sync({force, alter});
    await People.sync({force, alter});
    await Posters.sync({force, alter});
    // Teams
    // Tours
    await Users.sync({force, alter});

    // Таблицы со связями
    await Championships.sync({force, alter});
    await Tours.sync({force, alter});
    await Teams.sync({force, alter});

    await MediaFolders.sync({force, alter});
    await Media.sync({force, alter});

    await Matches.sync({force, alter});


};

module.exports.Models = {
    achievements: Achievements,
    brandBooks: BrandBooks,
    championships: Championships,
    dates: Dates,
    documents: Documents,
    mainSlides: MainSlides,
    massMedia: MassMedia,
    matches: Matches,
    media: Media,
    mediaFolders: MediaFolders,
    news: News,
    newsForFans: NewsForFans,
    paragraphs: Paragraphs,
    partners: Partners,
    people: People,
    posters: Posters,
    teams: Teams,
    tours: Tours,
    users: Users,
};
