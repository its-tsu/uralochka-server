const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");

const Model = Sequelize.Model;

class BrandBooks extends Model {
}

BrandBooks.init({
    imgUrl: fieldsCreator.FILE.imgUrl(),
    name: fieldsCreator.TEXT.name(),
}, {
    sequelize,
    paranoid: true,
    modelName: 'brand-books',
});

module.exports = {
    BrandBooks,
};