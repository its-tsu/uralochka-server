const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");

const Model = Sequelize.Model;

class Achievements extends Model {}

Achievements.init({
    secondImgUrl: fieldsCreator.FILE.secondImgUrl(),
    name: fieldsCreator.TEXT.name(),
    position: fieldsCreator.NUMBER.position(),
    firstParagraph: fieldsCreator.TEXT.text(),
    secondParagraph: fieldsCreator.TEXT.text({require: false}),
    championships: fieldsCreator.TEXT.text(),
}, {
    sequelize,
    paranoid: true,
    modelName: 'achievements',
});

module.exports = {
    Achievements,
};
