const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");

const Model = Sequelize.Model;

class NewsForFans extends Model {}

NewsForFans.init({
    title: fieldsCreator.TEXT.title(),
    text: fieldsCreator.TEXT.text(),
    date: fieldsCreator.DATE.date(),
    imgUrl: fieldsCreator.FILE.imgUrl(),
}, {
    sequelize,
    paranoid: true,
    modelName: 'news-for-fans',
});

module.exports = {
    NewsForFans,
};