const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");

const Model = Sequelize.Model;

class MainSlides extends Model {}

MainSlides.init({
    name: fieldsCreator.TEXT.name(),
    imgUrl: fieldsCreator.FILE.imgUrl(),
}, {
    sequelize,
    paranoid: true,
    modelName: 'mainSlides',
});

module.exports = {
    MainSlides,
};