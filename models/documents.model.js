const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");
const {types} = require("../constants/models/documents");

const Model = Sequelize.Model;

class Documents extends Model {
}

Documents.init({
    fileUrl: fieldsCreator.FILE.fileUrl(),
    name: fieldsCreator.TEXT.name(),
    type: fieldsCreator.TEXT.type({
        isIn: types,
    }),

}, {
    sequelize,
    paranoid: true,
    modelName: 'documents',
});

module.exports = {
    Documents,
};