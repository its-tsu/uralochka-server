const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const fieldsCreator = require("../libs/fieldsCreator");

const Model = Sequelize.Model;

class Teams extends Model {}

Teams.init({
    name: fieldsCreator.TEXT.name(),
    imgUrl: fieldsCreator.FILE.imgUrl(),
}, {
    sequelize,
    paranoid: true,
    modelName: 'teams',
});

module.exports = {
    Teams,
};